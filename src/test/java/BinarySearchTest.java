import org.junit.Assert;
import org.junit.Test;
import questions.BinarySearch;

public class BinarySearchTest {

  @Test
  public void givenSortedArrayShouldReturnIndex() {
    int[] array = new int[10];
    int number = 5;
    for (int i = 0; i < 10; i++) {
      array[i] = number;
      number += 9;
    }
    int expectedIndex = 1;
    int find = 14;
    Assert.assertEquals(expectedIndex, BinarySearch.binarySearch(array, find));
    expectedIndex = 2;
    find = 23;
    Assert.assertEquals(expectedIndex, BinarySearch.binarySearch(array, find));
    expectedIndex = 9;
    find = 86;
    Assert.assertEquals(expectedIndex, BinarySearch.binarySearch(array, find));
    expectedIndex = -1;
    find = 666;
    Assert.assertEquals(expectedIndex, BinarySearch.binarySearch(array, find));
  }
}
