import questions.RepeatedString;
import org.junit.Assert;
import org.junit.Test;

public class RepeatedStringTest {

  @Test
  public void shouldReturnNumberOfA() {
    String s = "aba";
    long n = 10;
    Assert.assertEquals(7, RepeatedString.repeatedString(s, n));

    s = "abfdghbvaafujhb";
    n = 987654588L;
    Assert.assertEquals(197530918, RepeatedString.repeatedString(s, n));

    s = "qwerty";
    n = 1000000000000L;
    Assert.assertEquals(0, RepeatedString.repeatedString(s, n));

    s = "qwertya";
    n = 1000000000000L;
    Assert.assertEquals(142857142857L, RepeatedString.repeatedString(s, n));

  }

}
