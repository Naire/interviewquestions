import static junit.framework.Assert.assertFalse;
import static junit.framework.TestCase.assertTrue;

import org.junit.Test;

import questions.Prime;

public class PrimeTest {

  @Test
  public void givenPrimeShouldReturnTrue() {
    int number = 97;
    assertTrue(Prime.isPrime(number));
    number = 7;
    assertTrue(Prime.isPrime(number));
    number = 43;
    assertTrue(Prime.isPrime(number));
  }
  @Test
  public void givenNotPrimeShouldReturnFalse() {
    int number = 42;
    assertFalse(Prime.isPrime(number));
    number = 0;
    assertFalse(Prime.isPrime(number));
    number = -7;
    assertFalse(Prime.isPrime(number));
  }
}
