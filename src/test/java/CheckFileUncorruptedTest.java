import org.junit.Assert;
import org.junit.Test;
import questions.CheckFileUncorrupted;

public class CheckFileUncorruptedTest {

  @Test
  public void givenNegativeShouldCountExtraOne() {
    long data = -2L;//1010
    Assert.assertFalse(CheckFileUncorrupted.hasEvenNumberOfOnes(data));
    data = -7L;//1111
    Assert.assertTrue(CheckFileUncorrupted.hasEvenNumberOfOnes(data));
  }

  @Test
  public void givenEvenNumberOfOnesShouldReturnTrue() {
    long data = 3L;//0011
    Assert.assertTrue(CheckFileUncorrupted.hasEvenNumberOfOnes(data));
  }

  @Test
  public void givenOddNumberOfOnesShouldReturnFalse() {
    long data = 8L;//1000
    Assert.assertFalse(CheckFileUncorrupted.hasEvenNumberOfOnes(data));
  }

}
