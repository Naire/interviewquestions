import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import java.util.Arrays;
import questions.ZeroSum;

public class ZeroSumTest {

  @Test
  public void givenValidNShouldReturnArrayWithSumZero() {
    int n = 20;
    int expected = 0;
    assertEquals(expected, Arrays.stream(ZeroSum.arrayOfOpposites(n)).sum());

    n = 99;
    assertEquals(expected, Arrays.stream(ZeroSum.arrayOfOpposites(n)).sum());
  }

  @Test(expected = IllegalArgumentException.class)
  public void givenInvalidNShouldThrow() {
    int n = -1;
    ZeroSum.arrayOfOpposites(n);
    fail();
  }
}
