import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import questions.ToBinary;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.junit.Test;


public class ToBinaryTest {

  @Test
  public void givenIntegerShouldReturnBinaryUsingLib() {
    long start, stop;
    int number = 300;
    String expected = "100101100";
    start = System.nanoTime();
    assertEquals(expected, ToBinary.toBinaryWithLib(number));
    stop = System.nanoTime();
    long time = (stop - start) / 1000;
    System.out.println("Time while using Integer.toBinaryString: " + time + " microseconds");
  }

  @Test
  public void givenIntegerShouldReturnBinaryUsingArrayDeque() {
    long start, stop;
    int number = 300;
    start = System.nanoTime();
    final ArrayDeque<Boolean> toCheck = ToBinary.toBinaryArrayDeque(number);
    stop = System.nanoTime(); 
    long time = (stop - start) / 1000;
    System.out.println("Time while using ArrayDeque: " + time + " microseconds");

    int i = 0;
    final List<Boolean> expected = Arrays
        .asList(true, false, false, true, false, true, true, false, false);
    for (Boolean digit : toCheck) {
      assertEquals(expected.get(i++), digit);
    }
  }

  @Test
  public void givenIntegerShouldReturnBinaryUsingLinkedList() {
    long start, stop;
    int number = 300;
    start = System.nanoTime();
    final LinkedList<Boolean> toCheck = ToBinary.toBinaryLinkedList(number);
    stop = System.nanoTime();
    long time = (stop - start) / 1000;
    System.out.println("Time while using LinkedList: " + time + " microseconds");
    int i = 0;
    final List<Boolean> expected = Arrays
        .asList(true, false, false, true, false, true, true, false, false);
    for (Boolean digit : toCheck) {
      assertEquals(expected.get(i++), digit);
    }
  }

  @Test
  public void givenIntegerShouldReturnBinaryUsingDivision() {
    long start, stop;
    int number = 300;
    String expected = "100101100";
    start = System.nanoTime();
    assertEquals(expected, ToBinary.toBinary(number));
    stop = System.nanoTime();
    long time = (stop - start) / 1000;
    System.out.println("Time while using division: " + time + " microseconds");
  }
  
  @Test
  public void givenTwoIntegersShouldReturnXor() {
    long start, stop;
    int number1 = 22, number2 = 162; //10110^10100010
    String expected = "10110100";
    start = System.nanoTime();
    assertEquals(expected, ToBinary.xorWithDiv(number1, number2));
    stop = System.nanoTime();
    long time = (stop - start) / 1000;
    System.out.println("Time calculating XOR while using division: " + time + " microseconds");
    assertEquals(expected, ToBinary.xorWithDiv(number2, number1));
  }
}