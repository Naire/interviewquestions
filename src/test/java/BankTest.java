import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.junit.Assert;
import org.junit.Test;
import questions.synchronization.bank.Bank;

public class BankTest {

  private static final int NUMBER_OF_ACCOUNTS = 1000;
  private static final double INITIAL_FUNDS = 1000;
  private static final double MAX_TRANSFER_AMOUNT = 100;
  private static final int NUMBER_OF_TASKS = 10000;
  private static final int DELAY = 5;
  private static final Double TOTAL_BALANCE = NUMBER_OF_ACCOUNTS * INITIAL_FUNDS;
  private final Bank bank;


  public BankTest() {
    bank = new Bank(NUMBER_OF_ACCOUNTS, INITIAL_FUNDS);
  }

  @Test
  public void sumShouldStayConstantWhileTransferring() throws InterruptedException {
    ExecutorService executor = Executors.newFixedThreadPool(10);
    List<Callable<Double>> tasks = new ArrayList<>(NUMBER_OF_TASKS);
    int i = 0;
    while (i++ < NUMBER_OF_TASKS) {
      tasks.add(getTransferTask());
    }
    final List<Future<Double>> balances = executor.invokeAll(tasks);
    executor.shutdown();
    assertFutures(balances);
  }

  private void assertFutures(List<Future<Double>> balances) throws InterruptedException {
    ExecutorService executor = Executors.newFixedThreadPool(10);
    List<Callable<Void>> tasks = new ArrayList<>(balances.size());
    for (Future<Double> balance : balances) {
      tasks.add(getAssertTask(balance));
    }
    executor.invokeAll(tasks);
    executor.shutdown();
  }

  private Callable<Void> getAssertTask(Future<Double> balance) {
    return () -> {
      while (!balance.isDone()) {
        Thread.sleep((long) (DELAY * Math.random()));
      }
      Assert.assertEquals(TOTAL_BALANCE, balance.get());
      return null;
    };
  }

  private Callable<Double> getTransferTask() {
    return () -> {
      int fromAccount = (int) (bank.getNumberOfAccounts() * Math.random());
      int toAccount;
      do {
        toAccount = (int) (bank.getNumberOfAccounts() * Math.random());
      } while (toAccount == fromAccount);
      double amount = MAX_TRANSFER_AMOUNT * Math.random();
      try {
        final double totalBalance = bank.transfer(fromAccount, toAccount, amount);
        Thread.sleep((long) (DELAY * Math.random()));
        return totalBalance;
      } catch (InterruptedException e) {
        System.out
            .printf("Thread: %s has been interrupted. Message: %s%n", Thread.currentThread(), e
                .getMessage());
      }
      return null;
    };
  }

}
