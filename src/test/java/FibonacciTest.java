import static org.junit.Assert.assertEquals;

import org.junit.Test;

import questions.Fibonacci;

public class FibonacciTest {

  @Test
  public void givenInvalidInputReturnsMinusOneIterative() {
    int expected = -1;
    int whichNumber = -5;
    assertEquals(expected, Fibonacci.iterative(whichNumber));
    whichNumber = 99;
    assertEquals(expected, Fibonacci.iterative(whichNumber));
  }

  @Test
  public void givenInvalidInputReturnsMinusOneRecursive() {
    long expected = -1;
    int whichNumber = -5;
    assertEquals(expected, Fibonacci.recursive(whichNumber));
    whichNumber = 99;
    assertEquals(expected, Fibonacci.recursive(whichNumber));
    whichNumber = 0;
    assertEquals(expected, Fibonacci.recursive(whichNumber));
  }

  @Test
  public void givenValidInputReturnsNthNumberIterative() {
    long expected = 1;
    int whichNumber = 1;
    assertEquals(expected, Fibonacci.iterative(whichNumber));
    expected = 7540113804746346429L;
    whichNumber = 92;
    assertEquals(expected, Fibonacci.iterative(whichNumber));
    expected = 144;
    whichNumber = 12;
    assertEquals(expected, Fibonacci.iterative(whichNumber));
  }

  @Test
  public void givenValidInputReturnsNthRecursive() {
    long expected = 1;
    int whichNumber = 2;
    assertEquals(expected, Fibonacci.recursive(whichNumber));
    expected = 832040;
    whichNumber = 30;
    assertEquals(expected, Fibonacci.recursive(whichNumber));
    expected = 13;
    whichNumber = 7;
    assertEquals(expected, Fibonacci.recursive(whichNumber));
  }
}
