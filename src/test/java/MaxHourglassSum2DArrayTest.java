import org.junit.Assert;
import org.junit.Test;
import questions.MaxHourglassSum2DArray;

public class MaxHourglassSum2DArrayTest {

  @Test
  public void givenArrayShouldReturnMaxHourglassSum() {
    int[][] arr = { //19
        {1, 1, 1, 0, 0, 0},
        {0, 1, 0, 0, 0, 0},
        {1, 1, 1, 0, 0, 0},
        {0, 0, 2, 4, 4, 0},
        {0, 0, 0, 2, 0, 0},
        {0, 0, 1, 2, 4, 0}
    };

    int expected = 19;
    Assert.assertEquals(expected, MaxHourglassSum2DArray.hourglassSum(arr));
  }

  @Test
  public void givenArrayWithNegativesShouldReturnMaxHourglassSum() {
    int[][] arr = {
        {-9, -9, -9, 1, 1, 1},
        {0, -9, 0, 4, 3, 2},
        {-9, -9, -9, 1, 2, 3},
        {0, 0, 8, 6, 6, 0},
        {0, 0, 0, -2, 0, 0},
        {0, 0, 1, 2, 4, 0}
    };

    int expected = 28;
    Assert.assertEquals(expected, MaxHourglassSum2DArray.hourglassSum(arr));
  }

}
