import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import questions.MaxThreeFromList;

public class MaxThreeFromListTest {
  private final List<Integer> list;
  private final List<Integer> expected;

  public MaxThreeFromListTest() {
    list = Arrays.asList(5,8,1029,334233,999999,1356,999,-2,-53,3,250,500,800,-4625,0,19991,2000,2);
    expected = Arrays.asList(999999, 334233, 19991);
  }

  @Test
  public void shouldReturnThreeMaxNumbersStream() {
    Assert.assertEquals(expected, MaxThreeFromList.getMaxThreeUsingStreams(list));
  }

  @Test
  public void shouldReturnThreeMaxNumbersCollections() {
    Assert.assertEquals(expected, MaxThreeFromList.getMaxThreeUsingCollectionSort(list));
  }

  @Test
  public void shouldReturnThreeMaxNumbersQuickSort() {
    Assert.assertEquals(expected, MaxThreeFromList.getMaxThreeUsingQuickSort(list));
  }
}
