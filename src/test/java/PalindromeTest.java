import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Assert;
import org.junit.Test;

import questions.Palindrome;

public class PalindromeTest {

  @Test
  public void givenPalindromeShouldReturnTrue() {
    String palindrome = "kajak";
    assertTrue(Palindrome.isPalindrome(palindrome));
    palindrome = "TacoCat";
    assertTrue(Palindrome.isPalindrome(palindrome));
  }

  @Test
  public void givenNotPalindromeShouldReturnFalse() {
    String data = "not a palindrome";
    assertFalse(Palindrome.isPalindrome(data));
    data = "taco cat";
    assertFalse(Palindrome.isPalindrome(data));
  }
}
