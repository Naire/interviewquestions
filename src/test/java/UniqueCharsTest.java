import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import questions.UniqueChars;

public class UniqueCharsTest {
  //Test String version
  @Test
  public void givenUniqueCharsShouldReturnYes() {
    String expected = "YES";
    String data = "A word.";
    assertEquals(expected, UniqueChars.areAllUniqueCharsStringVersion(data));
  }

  @Test
  public void givenNotUniqueCharsShouldReturnNo() {
    String expected = "NO";
    String data = "Not a word.";
    assertEquals(expected, UniqueChars.areAllUniqueCharsStringVersion(data));
  }

  @Test
  public void givenEmptyShouldReturnDataIsEmpty() {
    String expected = "Data is empty.";
    String data = "";
    assertEquals(expected, UniqueChars.areAllUniqueCharsStringVersion(data));
  }

  //Test boolean version
  @Test
  public void givenUniqueCharsShouldReturnTrue() {
    String data = "abcdefghijklmnopqrstuvwxyz";
    assertTrue(UniqueChars.areAllUniqueCharsBooleanVersion(data));
  }

  @Test
  public void givenNotUniqueCharsShouldReturnFalse() {
    String data = "Nothing unique about this sentence.";
    assertFalse(UniqueChars.areAllUniqueCharsBooleanVersion(data));
  }

  @Test
  public void givenEmptyShouldReturnTrue() {
    String data = "";
    assertTrue(UniqueChars.areAllUniqueCharsBooleanVersion(data));
    data = null;
    assertTrue(UniqueChars.areAllUniqueCharsBooleanVersion(data));
  }

  //Test remove duplicates
  @Test
  public void givenUniqueShouldReturnUnchanged() {
    String data = "qwerty";
    assertEquals(data, UniqueChars.removeDuplicateChars(data));
  }

  @Test
  public void givenNotUniqueShouldRemoveDuplicates() {
    String data = "There are some characters in this sentence that should be removed...";
    String expected = "Ther asomctinuldbv.";
    assertEquals(expected, UniqueChars.removeDuplicateChars(data));
  }

}
