package design.patterns.exercises.iterator;

import java.util.ArrayList;
import java.util.List;
import design.patterns.exercises.iterator.menu.DinerMenu;
import design.patterns.exercises.iterator.menu.Menu;
import design.patterns.exercises.iterator.menu.PancakeHouseMenu;
import design.patterns.exercises.iterator.waitress.Alice;
import design.patterns.exercises.iterator.waitress.Waitress;

/**
 * The Iterator Pattern provides a way to access the elements of an aggregate object sequentially
 * without exposing its underlying representation.
 */
public class DinerAndPancakeMerge {
  public static void main(String[] args) {
    System.out.println("Our Diner and Pancake House are merging!");
    List<Menu> menus = new ArrayList<>(2);
    menus.add(new DinerMenu());
    menus.add(new PancakeHouseMenu());

    Waitress waitress = new Alice(menus);
    waitress.printMenu();
    System.out.println("----------------------------");

    String[] items = {"Fries", "Hotdog", "NotInMenuItem", "Pancake delight"};
    for (String item : items) {
      vegeConversation(waitress, item);
    }
  }

  private static void vegeConversation(final Waitress waitress, final String itemName) {
    System.out.printf("Client: Is %s vegetarian?%n", itemName);
    System.out.println(waitress.isItemVege(itemName) ? "Alice: yes" : "Alice: no");
  }
}
