package design.patterns.exercises.iterator.menu;

import java.util.Iterator;

public class DinerIterator implements Iterator<MenuItem> {
  private int position = 0;
  private MenuItem[] items;

  public DinerIterator(MenuItem[] items) {
    this.items = items;
  }

  @Override
  public boolean hasNext() {
    return items.length > position && items[position] != null;
  }

  @Override
  public MenuItem next() {
    return items[position++];
  }

}
