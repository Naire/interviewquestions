package design.patterns.exercises.iterator.menu;

import java.util.ArrayList;
import java.util.Iterator;

public class PancakeHouseMenu implements Menu {
  final ArrayList<MenuItem> menuItems;

  public PancakeHouseMenu() {
    menuItems = new ArrayList<>();

    addItem("Pancake breakfast", "Pancakes with scrambled eggs and toast",
        true, 2.99);
    addItem("Pancake delight", "Pancakes with chocolate",
        true, 5.99);
    addItem("Pancake meaty mix", "Pancakes with bacon",
        false, 3.99);

  }

  @Override
  public Iterator<MenuItem> createIterator() {
    return menuItems.iterator();
  }

  public void addItem(String name, String description, boolean isVege, double price) {
    final MenuItem item = new MenuItem(name, description, isVege, price);
    if (!menuItems.contains(item)) {
      menuItems.add(item);
    }
  }
}
