package design.patterns.exercises.iterator.menu;

import java.util.Objects;

public class MenuItem {
  String name;
  String description;
  boolean vegetarian;
  double price;

  public MenuItem(String name, String description, boolean vegetarian, double price) {
    this.name = name;
    this.description = description;
    this.vegetarian = vegetarian;
    this.price = price;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public boolean isVegetarian() {
    return vegetarian;
  }

  public double getPrice() {
    return price;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder(name);
    sb.append(" - ").append(description);
    sb.append(" Vegetarian: ").append(vegetarian ? "yes" : "no");
    sb.append(". Price: ").append(price);
    return sb.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MenuItem menuItem = (MenuItem) o;
    return isVegetarian() == menuItem.isVegetarian() &&
        Double.compare(menuItem.getPrice(), getPrice()) == 0 &&
        getName().equals(menuItem.getName()) &&
        Objects.equals(getDescription(), menuItem.getDescription());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getName(), getDescription(), isVegetarian(), getPrice());
  }
}
