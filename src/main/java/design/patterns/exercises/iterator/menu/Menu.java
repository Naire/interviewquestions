package design.patterns.exercises.iterator.menu;

import java.util.Iterator;
import java.util.Optional;

public interface Menu {
  Iterator<MenuItem> createIterator();

  default Optional<MenuItem> findItemByName(String name) {
    final Iterator<MenuItem> iterator = createIterator();
    while (iterator.hasNext()) {
      final MenuItem item = iterator.next();
      if (item.getName().equals(name)) {
        return Optional.of(item);
      }
    }
    return Optional.empty();
  }
}
