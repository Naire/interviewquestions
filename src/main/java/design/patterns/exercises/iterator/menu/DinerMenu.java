package design.patterns.exercises.iterator.menu;

import java.util.Iterator;

public class DinerMenu implements Menu {
  static final int MAX_ITEMS = 6;
  int numberOfItems = 0;
  MenuItem[] menuItems;

  public DinerMenu() {
    menuItems = new MenuItem[MAX_ITEMS];
    addItem("Burger", "Yummy burger", false, 15.50);
    addItem("Fries", "French fries with ketchup", true, 5);
    addItem("Hotdog", "Hotdog with mustard", false, 3.30);
  }

  @Override
  public Iterator<MenuItem> createIterator() {
    return new DinerIterator(menuItems);
  }

  public void addItem(String name, String description, boolean isVege, double price) {
    if (numberOfItems >= MAX_ITEMS) {
      System.out.println("Menu is full. Unable to add new item.");
    } else {
      menuItems[numberOfItems] = new MenuItem(name, description, isVege, price);
      numberOfItems++;
    }
  }
}
