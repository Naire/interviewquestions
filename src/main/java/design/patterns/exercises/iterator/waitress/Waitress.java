package design.patterns.exercises.iterator.waitress;

public interface Waitress {
  void printMenu();

  void printVegeMenu();

  boolean isItemVege(String name);
}
