package design.patterns.exercises.iterator.waitress;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import design.patterns.exercises.iterator.menu.Menu;
import design.patterns.exercises.iterator.menu.MenuItem;

public class Alice implements Waitress {
  private List<Menu> menus;

  public Alice(List<Menu> menus) {
    this.menus = menus;
  }

  @Override
  public void printMenu() {
    System.out.println("---MENU---");
    menus.forEach(menu -> printMenu(menu.createIterator()));
  }

  @Override
  public void printVegeMenu() {
    System.out.println("Vege menu:");
    menus.stream().map(Menu::createIterator).forEach(iterator -> {
      while (iterator.hasNext()) {
        final MenuItem item = iterator.next();
        if (item.isVegetarian()) {
          System.out.println(item);
        }
      }
    });
  }

  @Override
  public boolean isItemVege(String name) {

    /*
    for (Menu menu : menus) {
      final Optional<MenuItem> itemByName = menu.findItemByName(name);
      if (itemByName.isPresent()) {
        return itemByName.get().isVegetarian();
      }
    }
    return false;*/

    //name should be unique in the menu
    return menus.stream().map(menu -> menu.findItemByName(name)).filter(Optional::isPresent)
        .findFirst().filter(itemByName -> itemByName.get().isVegetarian()).isPresent();
  }

  private void printMenu(Iterator<MenuItem> iterator) {
    while (iterator.hasNext()) {
      System.out.println(iterator.next());
    }
  }
}
