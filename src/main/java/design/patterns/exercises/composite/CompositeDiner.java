package design.patterns.exercises.composite;

import java.util.ArrayList;
import java.util.List;
import design.patterns.exercises.composite.menu.DinerMenu;
import design.patterns.exercises.composite.menu.Menu;
import design.patterns.exercises.composite.menu.MenuComponent;
import design.patterns.exercises.composite.menu.PancakeHouseMenu;
import design.patterns.exercises.composite.menu.ParentMenu;
import design.patterns.exercises.composite.waitress.Alice;
import design.patterns.exercises.composite.waitress.Waitress;

/**
 * The Composite Pattern allows building structures of objects in the form of trees that contain
 * both compositions of objects and individual objects as nodes.
 */
public class CompositeDiner {
  public static void main(String[] args) {
    final List<Menu> menus = new ArrayList<>();
    menus.add(new PancakeHouseMenu("Pancakes", "Pancake House"));
    menus.add(new DinerMenu("Dinner", "Diner House"));
    final MenuComponent compositeDinerMenu = new ParentMenu("Composite Diner", "Our Menu", menus);
    final Waitress waitress = new Alice(compositeDinerMenu);
    waitress.printMenu();
    System.out.println("------------------------------");
    waitress.printVegeMenu();
    System.out.println("-------------Conversation with Alice-----------------");
    System.out.println("Are 'Fries' vegetarian?");
    System.out.println(waitress.isItemVege("Fries") ? "yes" : "no");
    System.out.println("Is 'Hotdog' vegetarian? :)");
    System.out.println(waitress.isItemVege("Hotdog") ? "yes" : "no");
    System.out.println("Are 'Ice cream' vegetarian?");
    System.out.println(waitress.isItemVege("Ice cream") ? "yes" : "no");
  }
}
