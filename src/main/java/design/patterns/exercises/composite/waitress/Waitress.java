package design.patterns.exercises.composite.waitress;

public interface Waitress {
  void printMenu();

  void printVegeMenu();

  boolean isItemVege(String name);
}
