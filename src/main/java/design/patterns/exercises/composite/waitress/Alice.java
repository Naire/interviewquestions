package design.patterns.exercises.composite.waitress;

import java.util.List;
import design.patterns.exercises.composite.menu.MenuComponent;
import design.patterns.exercises.composite.menu.MenuItem;

public class Alice implements Waitress {
  private MenuComponent allMenus;
  private boolean isVegeAnswer;

  public Alice(MenuComponent allMenus) {
    this.allMenus = allMenus;
  }

  @Override
  public void printMenu() {
    allMenus.print();
  }


  @Override
  public void printVegeMenu() {
    allMenus.printVegetarian();
  }

  @Override
  public boolean isItemVege(String name) {
    setIsVege(allMenus.getChildren(), name);
    boolean answer = isVegeAnswer;
    isVegeAnswer = false;
    return answer;
  }

  private void setIsVege(List<MenuComponent> children, String name) {
    for (MenuComponent menuComponent : children) {
      if (menuComponent instanceof MenuItem) {
        if (menuComponent.getName().equals(name)) {
          isVegeAnswer = menuComponent.isVegetarian();
        }
      } else {
        setIsVege(menuComponent.getChildren(), name);
      }
    }
  }

}
