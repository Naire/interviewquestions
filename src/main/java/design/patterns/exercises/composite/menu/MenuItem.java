package design.patterns.exercises.composite.menu;

public class MenuItem extends Menu {
  private boolean isVege;
  private double price;

  public MenuItem(final String name, final String description, final boolean isVege,
                  final double price) {
    super(name, description);
    this.isVege = isVege;
    this.price = price;
  }

  @Override
  public void print() {
    StringBuilder sb = new StringBuilder(getName());
    sb.append(" - ").append(getDescription());
    sb.append(", price: ").append(getPrice());
    sb.append("\n Vege: ");
    sb.append(isVegetarian() ? "yes" : "no");
    sb.append("\n");
    System.out.println(sb);
  }

  @Override
  public double getPrice() throws UnsupportedOperationException {
    return this.price;
  }

  @Override
  public boolean isVegetarian() throws UnsupportedOperationException {
    return this.isVege;
  }

  @Override
  public void printVegetarian() {
    if (isVege) {
      System.out.printf("%s - %s, price: %s%n", getName(), getDescription(), getPrice());
    }
  }
}
