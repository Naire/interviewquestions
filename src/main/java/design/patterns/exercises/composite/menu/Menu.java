package design.patterns.exercises.composite.menu;

import java.util.ArrayList;
import java.util.List;

public class Menu extends MenuComponent {
  protected final List<MenuComponent> children;
  private String name;
  private String description;

  public Menu(final String name, final String description) {
    this.name = name;
    this.description = description;
    children = new ArrayList<>();
  }

  @Override
  public boolean add(MenuComponent menuComponent) {
    if (!children.contains(menuComponent)) {
      children.add(menuComponent);
      return true;
    }
    return false;
  }

  @Override
  public boolean remove(MenuComponent menuComponent) {
    return children.remove(menuComponent);
  }

  @Override
  public MenuComponent getChild(int whichChild) {
    return children.get(whichChild);
  }

  @Override
  public List<MenuComponent> getChildren() throws UnsupportedOperationException {
    return new ArrayList<>(children);
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public String getDescription() {
    return this.description;
  }

  @Override
  public void print() {
    System.out.printf("%n%s, %s%n", getName(), getDescription());
    System.out.println("--------------------");
    children.forEach(MenuComponent::print);
  }

  @Override
  public void printVegetarian() {
    System.out.println("Vegetarian items");
    printVege(children);
  }

  private void printVege(List<MenuComponent> children) {
    children.forEach(menuComponent -> {
      if (menuComponent instanceof MenuItem) {
        menuComponent.printVegetarian();
      } else {
        printVege(menuComponent.getChildren());
      }
    });
  }
}
