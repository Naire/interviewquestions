package design.patterns.exercises.composite.menu;

import java.util.List;

/**
 * The role of the menu component is to provide an interface for the leaf nodes and the composite
 * nodes.
 */
public abstract class MenuComponent {

  /*Composite methods*/
  public boolean add(MenuComponent menuComponent) throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  public boolean remove(MenuComponent menuComponent) throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  public MenuComponent getChild(int whichChild) throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  public List<MenuComponent> getChildren() throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /*Operation methods*/
  public String getName() throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  public String getDescription() throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  public double getPrice() throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  public boolean isVegetarian() throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  public void printVegetarian() throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  public void print() throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

}
