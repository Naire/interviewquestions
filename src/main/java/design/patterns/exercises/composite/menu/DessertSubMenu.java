package design.patterns.exercises.composite.menu;

public class DessertSubMenu extends Menu {
  public DessertSubMenu(final String name, final String description) {
    super(name, description);
    addDefaultItems();
  }

  private void addDefaultItems() {
    add(new MenuItem("Ice cream", "Vanilla with cream", true, 2.5));
    add(new MenuItem("Cheesecake", "With chocolate", true, 2.75));
  }

}
