package design.patterns.exercises.composite.menu;

public class PancakeHouseMenu extends Menu {

  public PancakeHouseMenu(String name, String description) {
    super(name, description);
    addDefaultItems();
  }

  private void addDefaultItems() {
    add(new MenuItem("Pancake breakfast", "Pancakes with scrambled eggs and toast",
        true, 2.99));
    add(new MenuItem("Pancake delight", "Pancakes with chocolate",
        true, 5.99));
    add(new MenuItem("Pancake meaty mix", "Pancakes with bacon",
        false, 3.99));
  }
}
