package design.patterns.exercises.composite.menu;

import java.util.List;

public class ParentMenu extends Menu {
  public ParentMenu(String name, String description, List<? extends MenuComponent> menus) {
    super(name, description);
    menus.forEach(this::add);
  }
}
