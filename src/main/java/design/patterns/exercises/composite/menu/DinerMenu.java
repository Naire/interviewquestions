package design.patterns.exercises.composite.menu;

public class DinerMenu extends Menu {
  static final int MAX_ITEMS = 6;
  int numberOfItems = 0;

  public DinerMenu(String name, String description) {
    super(name, description);
    addDefaultItems();
  }

  private void addDefaultItems() {
    addItem("Burger", "Yummy burger", false, 15.50);
    addItem("Fries", "French fries with ketchup", true, 5);
    addItem("Hotdog", "Hotdog with mustard", false, 3.30);
    add(new DessertSubMenu("Desserts", "Yummy desserts from the Diner"));
  }

  private void addItem(String name, String description, boolean isVege, double price) {
    if (numberOfItems >= MAX_ITEMS) {
      System.out.println("Menu is full. Unable to add new item.");
    } else {
      if (add(new MenuItem(name, description, isVege, price))) {
        numberOfItems++;
      }
    }
  }
}
