package design.patterns.exercises.strategy.animals.birds;

import design.patterns.exercises.strategy.animals.Animal;

public abstract class Bird extends Animal {
  private static final String typeOfAnimal = "It's a bird!";

  public Bird() {
    setTypeOfAnimal(typeOfAnimal);
  }
}
