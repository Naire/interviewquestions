package design.patterns.exercises.strategy.animals.birds;


import design.patterns.exercises.strategy.behaviours.movement.Dive;
import design.patterns.exercises.strategy.behaviours.movement.FlyWithWings;
import design.patterns.exercises.strategy.behaviours.movement.WalkOnTwoLegs;
import design.patterns.exercises.strategy.behaviours.sound.Quack;

public class ScaupDuck extends Duck {
  public ScaupDuck() {
    this.walkBehaviour = new WalkOnTwoLegs();
    this.flyBehaviour = new FlyWithWings();
    this.quackSound = new Quack();
    this.swimBehaviour = new Dive();
  }

  public void display() {
    System.out.println("Real scaup duck is here. Its name is " + getName());
  }
}
