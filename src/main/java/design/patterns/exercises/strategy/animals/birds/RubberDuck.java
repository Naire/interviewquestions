package design.patterns.exercises.strategy.animals.birds;


import design.patterns.exercises.strategy.behaviours.movement.Floating;
import design.patterns.exercises.strategy.behaviours.movement.StayStill;
import design.patterns.exercises.strategy.behaviours.sound.Squeak;

public class RubberDuck extends Duck {

  public RubberDuck() {
    this.walkBehaviour = new StayStill();
    this.flyBehaviour = new StayStill();
    this.quackSound = new Squeak();
    this.swimBehaviour = new Floating();
  }

  public void display() {
    System.out.println("Just a toy duck here :) Its name is " + getName());
  }
}
