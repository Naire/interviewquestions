package design.patterns.exercises.strategy.animals.birds;


import design.patterns.exercises.strategy.behaviours.movement.FlyBehaviour;
import design.patterns.exercises.strategy.behaviours.movement.SwimBehaviour;
import design.patterns.exercises.strategy.behaviours.movement.WalkBehaviour;
import design.patterns.exercises.strategy.behaviours.sound.QuackSound;

public abstract class Duck extends Bird {
  WalkBehaviour walkBehaviour;
  FlyBehaviour flyBehaviour;
  QuackSound quackSound;
  SwimBehaviour swimBehaviour;

  public void walk() {
    walkBehaviour.walk();
  }

  public void fly() {
    flyBehaviour.fly();
  }

  public void quack() {
    quackSound.quack();
  }

  public void swim() {
    swimBehaviour.swim();
  }

  public void setWalkBehaviour(WalkBehaviour walkBehaviour) {
    this.walkBehaviour = walkBehaviour;
  }

  public void setFlyBehaviour(FlyBehaviour flyBehaviour) {
    this.flyBehaviour = flyBehaviour;
  }

  public void setQuackSound(QuackSound quackSound) {
    this.quackSound = quackSound;
  }

  public void setSwimBehaviour(SwimBehaviour swimBehaviour) {
    this.swimBehaviour = swimBehaviour;
  }
}
