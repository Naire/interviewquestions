package design.patterns.exercises.strategy.animals.birds;


import design.patterns.exercises.strategy.behaviours.movement.Floating;
import design.patterns.exercises.strategy.behaviours.movement.FlyWithWings;
import design.patterns.exercises.strategy.behaviours.movement.WalkOnTwoLegs;
import design.patterns.exercises.strategy.behaviours.sound.Quack;

public class MallardDuck extends Duck {
  public MallardDuck() {
    this.walkBehaviour = new WalkOnTwoLegs();
    this.flyBehaviour = new FlyWithWings();
    this.quackSound = new Quack();
    this.swimBehaviour = new Floating();
  }

  public void display() {
    System.out.println("Real mallard duck is here. Its name is " + getName());
  }


}
