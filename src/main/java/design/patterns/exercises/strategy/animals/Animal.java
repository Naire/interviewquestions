package design.patterns.exercises.strategy.animals;

public abstract class Animal {
  private String typeOfAnimal = "Unknown";
  private String name = "Unnamed";

  public abstract void display();

  public String getTypeOfAnimal() {
    return typeOfAnimal;
  }

  protected void setTypeOfAnimal(String typeOfAnimal) {
    this.typeOfAnimal = typeOfAnimal;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
