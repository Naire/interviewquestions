package design.patterns.exercises.strategy;


import design.patterns.exercises.strategy.animals.birds.Duck;
import design.patterns.exercises.strategy.animals.birds.MallardDuck;
import design.patterns.exercises.strategy.animals.birds.RubberDuck;
import design.patterns.exercises.strategy.animals.birds.ScaupDuck;

/**
 * The Strategy Pattern defines a family of algorithms, encapsulates each one, and makes them
 * interchangeable. Strategy lets the algorithm vary independently from clients that use it.
 *
 * The classes "-Behaviour" are the algorithms, the set method lets them be changed.
 */
public class LittleZooSimulator {

  public static void main(String[] args) {
    System.out.println("Welcome in the little zoo simulator ;)");
    System.out.println("Here are the ducks!");
    duckSpectacle(new MallardDuck());
    duckSpectacle(new ScaupDuck());
    duckSpectacle(new RubberDuck());
  }

  private static void duckSpectacle(Duck duck) {
    duck.display();
    duck.quack();
    duck.fly();
    duck.walk();
    duck.swim();

  }
}
