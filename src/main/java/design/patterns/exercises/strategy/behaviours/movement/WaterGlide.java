package design.patterns.exercises.strategy.behaviours.movement;

public class WaterGlide implements SwimBehaviour {
  public void swim() {
    System.out.println("Swooshed over water.");
  }
}
