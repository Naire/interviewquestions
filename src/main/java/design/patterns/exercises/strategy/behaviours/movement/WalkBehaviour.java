package design.patterns.exercises.strategy.behaviours.movement;

public interface WalkBehaviour {
  void walk();
}
