package design.patterns.exercises.strategy.behaviours.movement;

public class HighJump implements JumpBehaviour {
  public void jump() {
    System.out.println("Jumped really high!");
  }
}
