package design.patterns.exercises.strategy.behaviours.sound;

public class Squeak implements QuackSound {
  public void quack() {
    System.out.println("Squeak squeak!");
  }
}
