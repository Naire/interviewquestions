package design.patterns.exercises.strategy.behaviours.movement;

public class Pounce implements JumpBehaviour {
  public void jump() {
    System.out.println("Pounced!");
  }
}
