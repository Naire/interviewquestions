package design.patterns.exercises.strategy.behaviours.sound;

public class Woof implements BarkSound {
  public void bark() {
    System.out.println("Woof woof woof!");
  }
}
