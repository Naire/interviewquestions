package design.patterns.exercises.strategy.behaviours.movement;

public class Dive implements SwimBehaviour {
  public void swim() {
    System.out.println("Dived in!");
  }
}
