package design.patterns.exercises.strategy.behaviours.movement;

public class StayStill implements WalkBehaviour, FlyBehaviour {
  public void walk() {
    stayStill();
  }

  public void fly() {
    stayStill();
  }

  private void stayStill() {
    System.out.println("<< stays completely still>>");
  }
}
