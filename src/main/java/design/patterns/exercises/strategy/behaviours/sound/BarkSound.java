package design.patterns.exercises.strategy.behaviours.sound;

public interface BarkSound {
  void bark();
}
