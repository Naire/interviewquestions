package design.patterns.exercises.strategy.behaviours.movement;

public class Floating implements SwimBehaviour {
  public void swim() {
    System.out.println("Floating without a care in the world...");
  }
}
