package design.patterns.exercises.strategy.behaviours.sound;

public class Quack implements QuackSound {
  public void quack() {
    System.out.println("Quack quack!");
  }
}
