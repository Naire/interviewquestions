package design.patterns.exercises.strategy.behaviours.sound;

public class Bark implements BarkSound {

  public void bark() {
    System.out.println("Bark bark bark!");
  }
}
