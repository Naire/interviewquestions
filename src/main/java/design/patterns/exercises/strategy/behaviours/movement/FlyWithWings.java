package design.patterns.exercises.strategy.behaviours.movement;

public class FlyWithWings implements FlyBehaviour {
  public void fly() {
    System.out.println("Flying using wings!");
  }
}
