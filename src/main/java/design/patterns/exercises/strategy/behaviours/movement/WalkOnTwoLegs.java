package design.patterns.exercises.strategy.behaviours.movement;

public class WalkOnTwoLegs implements WalkBehaviour {
  public void walk() {
    System.out.println("Step, step with two legs!");
  }
}
