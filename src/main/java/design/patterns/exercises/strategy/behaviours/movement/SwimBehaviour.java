package design.patterns.exercises.strategy.behaviours.movement;

public interface SwimBehaviour {
  void swim();
}
