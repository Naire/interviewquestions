package design.patterns.exercises.strategy.behaviours.movement;

public interface JumpBehaviour {
  void jump();
}
