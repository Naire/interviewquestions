package design.patterns.exercises.strategy.behaviours.movement;

public interface FlyBehaviour {
  void fly();
}
