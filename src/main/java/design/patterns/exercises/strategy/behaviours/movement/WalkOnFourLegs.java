package design.patterns.exercises.strategy.behaviours.movement;

public class WalkOnFourLegs implements WalkBehaviour {
  public void walk() {
    System.out.println("Step, step, step, step with four legs!");
  }
}
