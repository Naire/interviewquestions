package design.patterns.exercises.strategy.behaviours.sound;

public interface QuackSound {
  void quack();
}
