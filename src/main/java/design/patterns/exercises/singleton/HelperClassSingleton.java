package design.patterns.exercises.singleton;

/**
 * Implementation using a nested static class - created by Bill Pugh prior to Java 5.
 *
 * Lazy load. Does not require synchronization (high performance). Cannot be used for non static
 * class fields.
 */
public class HelperClassSingleton {

  private HelperClassSingleton() {
  }

  public static HelperClassSingleton getInstance() {
    return SingletonHolder.INSTANCE;
  }

  private static class SingletonHolder {

    private static final HelperClassSingleton INSTANCE = new HelperClassSingleton();
  }
}
