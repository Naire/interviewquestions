package design.patterns.exercises.singleton;

/**
 * Singleton as an enum. Cannot be intercepted by reflection - Java ensures that any enum value is
 * instantiated only once in a Java program. Doesn't allow lazy load. Suggested by Joshua Bloch.
 */
public enum EnumSingleton {
  INSTANCE;

  public static void doSomething() {
    System.out.println("Singleton method.");
  }
}
