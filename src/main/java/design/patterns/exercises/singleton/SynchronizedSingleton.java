package design.patterns.exercises.singleton;

/**
 * A thread safe implementation of lazy load singleton. Low performance - only one thread at a
 * time.
 */
public class SynchronizedSingleton {

  private static SynchronizedSingleton instance;

  private SynchronizedSingleton() {
  }

  public static synchronized SynchronizedSingleton getInstance() {
    if (instance == null) {
      instance = new SynchronizedSingleton();
    }
    return instance;
  }

}
