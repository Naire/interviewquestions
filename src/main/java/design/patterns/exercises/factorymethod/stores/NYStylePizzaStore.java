package design.patterns.exercises.factorymethod.stores;

import java.util.ArrayList;
import java.util.Optional;
import java.util.TreeMap;
import java.util.function.Supplier;
import design.patterns.exercises.factorymethod.ingredients.PizzaIngredientsFactory;
import design.patterns.exercises.factorymethod.ingredients.factories.NYPizzaIngredientsFactory;
import design.patterns.exercises.factorymethod.pizzas.Pizza;
import design.patterns.exercises.factorymethod.pizzas.nystyle.NyStyleCheesePizza;
import design.patterns.exercises.factorymethod.pizzas.nystyle.NyStylePepperoniPizza;

public class NYStylePizzaStore extends PizzaStores {
  private static NYPizzaIngredientsFactory factory;
  private final TreeMap<String, Supplier<Pizza>> inventory = new TreeMap<>();

  public NYStylePizzaStore() {
    inventory.put("NYSTYLECHEESEPIZZA",
        () -> new NyStyleCheesePizza(getNYIngredientFactory()));
    inventory.put("NYSTYLEPEPPERONIPIZZA",
        () -> new NyStylePepperoniPizza(getNYIngredientFactory()));
  }

  private static PizzaIngredientsFactory getNYIngredientFactory() {
    return factory == null ? factory = new NYPizzaIngredientsFactory() : factory;
  }

  @Override
  Optional<Pizza> createPizza(String type) {
    Supplier<Pizza> pizza = inventory.get(type.toUpperCase());
    return Optional.of(pizza.get());
  }

  public ArrayList<String> getListOfPossiblePizzas() {
    return new ArrayList<>(inventory.keySet());
  }
}
