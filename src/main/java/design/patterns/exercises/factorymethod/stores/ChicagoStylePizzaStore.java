package design.patterns.exercises.factorymethod.stores;

import java.util.ArrayList;
import java.util.Optional;
import java.util.TreeMap;
import java.util.function.Supplier;
import design.patterns.exercises.factorymethod.ingredients.PizzaIngredientsFactory;
import design.patterns.exercises.factorymethod.ingredients.factories.ChicagoPizzaIngredientsFactory;
import design.patterns.exercises.factorymethod.pizzas.Pizza;
import design.patterns.exercises.factorymethod.pizzas.chicagostyle.ChicagoStyleCheesePizza;
import design.patterns.exercises.factorymethod.pizzas.chicagostyle.ChicagoStylePepperoniPizza;

public class ChicagoStylePizzaStore extends PizzaStores {
  private static ChicagoPizzaIngredientsFactory factory;
  private final TreeMap<String, Supplier<Pizza>> inventory = new TreeMap<>();

  public ChicagoStylePizzaStore() {
    inventory.put("CHICAGOSTYLECHEESEPIZZA",
        () -> new ChicagoStyleCheesePizza(getChicagoIngredientFactory()));
    inventory.put("CHICAGOSTYLEPEPPERONIPIZZA",
        () -> new ChicagoStylePepperoniPizza(getChicagoIngredientFactory()));
  }

  private static PizzaIngredientsFactory getChicagoIngredientFactory() {
    return factory == null ? factory = new ChicagoPizzaIngredientsFactory() : factory;
  }

  @Override
  Optional<Pizza> createPizza(String type) {
    Supplier<Pizza> pizza = inventory.get(type.toUpperCase());
    return Optional.of(pizza.get());
  }

  public ArrayList<String> getListOfPossiblePizzas() {
    return new ArrayList<>(inventory.keySet());
  }
}
