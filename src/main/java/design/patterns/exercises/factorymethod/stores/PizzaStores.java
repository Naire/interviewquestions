package design.patterns.exercises.factorymethod.stores;

import java.util.Optional;
import design.patterns.exercises.factorymethod.exceptions.PizzaTypeNotFoundException;
import design.patterns.exercises.factorymethod.pizzas.Pizza;

public abstract class PizzaStores {
  public Pizza orderPizza(String type) throws PizzaTypeNotFoundException {
    Optional<Pizza> optionalPizza = createPizza(type);
    optionalPizza.orElseThrow(
        () -> new PizzaTypeNotFoundException(String.format("There is no pizza of type %s", type)));
    return optionalPizza.get().prepare().bake().cut().box();
  }

  /*This is the factory method*/
  abstract Optional<Pizza> createPizza(String type);
}
