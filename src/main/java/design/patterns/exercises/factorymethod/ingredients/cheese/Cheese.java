package design.patterns.exercises.factorymethod.ingredients.cheese;

import design.patterns.exercises.factorymethod.ingredients.Ingredient;

public abstract class Cheese implements Ingredient {
  abstract String getKindOfCheese();

  @Override
  public String toString() {
    return String.format("Cheese{%s}", getKindOfCheese());
  }
}
