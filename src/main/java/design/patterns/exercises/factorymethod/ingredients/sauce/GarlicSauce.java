package design.patterns.exercises.factorymethod.ingredients.sauce;

public class GarlicSauce extends Sauce {
  @Override
  public Sauce prepareSauce() {
    setColor("white");
    setTaste("garlic");
    return this;
  }

  @Override
  public String getIngredientName() {
    return this.getClass().getName();
  }
}
