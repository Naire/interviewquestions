package design.patterns.exercises.factorymethod.ingredients.veggie;

import design.patterns.exercises.factorymethod.ingredients.Ingredient;

public abstract class Veggie implements Ingredient {
  abstract String getVegetableType();

  @Override
  public String toString() {
    return String.format("Veggie{%s}", getVegetableType());
  }
}
