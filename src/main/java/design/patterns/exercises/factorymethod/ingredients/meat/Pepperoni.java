package design.patterns.exercises.factorymethod.ingredients.meat;

import design.patterns.exercises.factorymethod.ingredients.Ingredient;

public abstract class Pepperoni implements Ingredient {
  abstract String getTypeOfPepperoni();

  @Override
  public String toString() {
    return String.format("Pepperoni{%s}", getTypeOfPepperoni());
  }
}
