package design.patterns.exercises.factorymethod.ingredients.dough;

public class ThickDough extends Dough {
  private static final String type = "thick";

  public ThickDough() {
    setTypeOfDough(type);
  }

  @Override
  public Dough takeFlour() {
    System.out.println("Takes a lot of flour...");
    setTypeOfFlour("type 00");
    return this;
  }

  @Override
  public Dough addWater() {
    System.out.println("Adds water to the pot");
    return this;
  }

  @Override
  public Dough mix() {
    System.out.println("Mixing the dough very thoroughly");
    return this;
  }

  @Override
  public String getIngredientName() {
    return this.getClass().getName();
  }
}
