package design.patterns.exercises.factorymethod.ingredients;

public interface Ingredient {
  String getIngredientName();
}
