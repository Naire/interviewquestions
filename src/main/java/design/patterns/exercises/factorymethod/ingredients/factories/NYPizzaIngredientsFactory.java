package design.patterns.exercises.factorymethod.ingredients.factories;

import java.util.ArrayList;
import java.util.Collection;
import design.patterns.exercises.factorymethod.ingredients.PizzaIngredientsFactory;
import design.patterns.exercises.factorymethod.ingredients.cheese.Cheese;
import design.patterns.exercises.factorymethod.ingredients.cheese.MozzarellaCheese;
import design.patterns.exercises.factorymethod.ingredients.dough.Dough;
import design.patterns.exercises.factorymethod.ingredients.dough.ThinDough;
import design.patterns.exercises.factorymethod.ingredients.meat.Clams;
import design.patterns.exercises.factorymethod.ingredients.meat.FreshClams;
import design.patterns.exercises.factorymethod.ingredients.meat.Pepperoni;
import design.patterns.exercises.factorymethod.ingredients.meat.SlicedPepperoni;
import design.patterns.exercises.factorymethod.ingredients.sauce.Sauce;
import design.patterns.exercises.factorymethod.ingredients.sauce.TomatoSauce;
import design.patterns.exercises.factorymethod.ingredients.veggie.Spinach;
import design.patterns.exercises.factorymethod.ingredients.veggie.Tomato;
import design.patterns.exercises.factorymethod.ingredients.veggie.Veggie;

public class NYPizzaIngredientsFactory implements PizzaIngredientsFactory {
  @Override
  public Dough createDough() {
    return new ThinDough();
  }

  @Override
  public Sauce createSauce() {
    return new TomatoSauce().prepareSauce();
  }

  @Override
  public Cheese createCheese() {
    return new MozzarellaCheese();
  }

  @Override
  public Collection<Veggie> createVeggies(Integer numberOfVeggies) {
    Collection<Veggie> veggies = new ArrayList<>(numberOfVeggies);
    for (int i = 0; i < numberOfVeggies / 2; i++) {
      veggies.add(new Tomato());
      veggies.add(new Spinach());
    }
    return veggies;
  }

  @Override
  public Pepperoni createPepperoni() {
    return new SlicedPepperoni();
  }

  @Override
  public Clams createClam() {
    return new FreshClams();
  }
}
