package design.patterns.exercises.factorymethod.ingredients.dough;

public class ThinDough extends Dough {

  private Integer waterInLitres;

  public ThinDough() {
    setTypeOfDough("thin");
    setTypeOfFlour("wheat and mix");
    waterInLitres = 10;
  }

  @Override
  public Dough takeFlour() {
    System.out.printf("Measuring %s flour in the pot%n", getTypeOfFlour());
    return this;
  }

  @Override
  public Dough addWater() {
    System.out.printf("Adding %d litres of water to the pot%n", waterInLitres);
    return this;
  }

  @Override
  public Dough mix() {
    System.out.printf("Mixing and creating %s dough%n", getTypeOfDough());
    return this;
  }

  public Integer getWaterInLitres() {
    return waterInLitres;
  }

  public void setWaterInLitres(Integer waterInLitres) {
    this.waterInLitres = waterInLitres;
  }

  @Override
  public String getIngredientName() {
    return "thin";
  }
}
