package design.patterns.exercises.factorymethod.ingredients.sauce;

import design.patterns.exercises.factorymethod.ingredients.Ingredient;

public abstract class Sauce implements Ingredient {
  private String color;
  private String taste;

  public abstract Sauce prepareSauce();

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getTaste() {
    return taste;
  }

  public void setTaste(String taste) {
    this.taste = taste;
  }

  @Override
  public String toString() {
    return "Sauce{" +
        "color='" + color + '\'' +
        ", taste='" + taste + '\'' +
        '}';
  }
}
