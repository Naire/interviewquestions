package design.patterns.exercises.factorymethod.ingredients.dough;

import design.patterns.exercises.factorymethod.ingredients.Ingredient;

public abstract class Dough implements Ingredient {
  private String typeOfDough = "standard";
  private String typeOfFlour = "wheat";

  public String getTypeOfDough() {
    return typeOfDough;
  }

  protected void setTypeOfDough(String typeOfDough) {
    this.typeOfDough = typeOfDough;
  }

  public String getTypeOfFlour() {
    return typeOfFlour;
  }

  protected void setTypeOfFlour(String typeOfFlour) {
    this.typeOfFlour = typeOfFlour;
  }

  public Dough makeDough() {
    System.out.printf("Making %s dough%n", typeOfDough);
    return takeFlour().addWater().mix();
  }

  public abstract Dough takeFlour();

  public abstract Dough addWater();

  public abstract Dough mix();

  @Override
  public String toString() {
    return "Dough{" +
        "typeOfDough='" + typeOfDough + '\'' +
        ", typeOfFlour='" + typeOfFlour + '\'' +
        '}';
  }
}
