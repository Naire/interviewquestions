package design.patterns.exercises.factorymethod.ingredients.veggie;

public class Spinach extends Veggie {
  @Override
  public String getVegetableType() {
    return "green spinach";
  }

  @Override
  public String getIngredientName() {
    return "spinach";
  }
}
