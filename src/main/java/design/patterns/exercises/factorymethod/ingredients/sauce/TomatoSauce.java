package design.patterns.exercises.factorymethod.ingredients.sauce;

public class TomatoSauce extends Sauce {
  @Override
  public Sauce prepareSauce() {
    setColor("red");
    setTaste("tomato");
    return this;
  }

  @Override
  public String getIngredientName() {
    return this.getClass().getName();
  }
}
