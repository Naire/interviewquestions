package design.patterns.exercises.factorymethod.ingredients.factories;

import java.util.ArrayList;
import java.util.Collection;
import design.patterns.exercises.factorymethod.ingredients.PizzaIngredientsFactory;
import design.patterns.exercises.factorymethod.ingredients.cheese.Cheese;
import design.patterns.exercises.factorymethod.ingredients.cheese.ParmesanCheese;
import design.patterns.exercises.factorymethod.ingredients.dough.Dough;
import design.patterns.exercises.factorymethod.ingredients.dough.ThickDough;
import design.patterns.exercises.factorymethod.ingredients.meat.Clams;
import design.patterns.exercises.factorymethod.ingredients.meat.FrozenClams;
import design.patterns.exercises.factorymethod.ingredients.meat.Pepperoni;
import design.patterns.exercises.factorymethod.ingredients.meat.SlicedPepperoni;
import design.patterns.exercises.factorymethod.ingredients.sauce.GarlicSauce;
import design.patterns.exercises.factorymethod.ingredients.sauce.Sauce;
import design.patterns.exercises.factorymethod.ingredients.veggie.Tomato;
import design.patterns.exercises.factorymethod.ingredients.veggie.Veggie;

public class ChicagoPizzaIngredientsFactory implements PizzaIngredientsFactory {
  @Override
  public Dough createDough() {
    return new ThickDough();
  }

  @Override
  public Sauce createSauce() {
    return new GarlicSauce().prepareSauce();
  }

  @Override
  public Cheese createCheese() {
    return new ParmesanCheese();
  }

  @Override
  public Collection<Veggie> createVeggies(Integer numberOfVeggies) {
    Collection<Veggie> veggies = new ArrayList<>(numberOfVeggies);
    for (int i = 0; i < numberOfVeggies; i++) {
      veggies.add(new Tomato());
    }
    return veggies;
  }

  @Override
  public Pepperoni createPepperoni() {
    return new SlicedPepperoni();
  }

  @Override
  public Clams createClam() {
    return new FrozenClams();
  }
}
