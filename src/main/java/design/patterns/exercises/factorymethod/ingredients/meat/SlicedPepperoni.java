package design.patterns.exercises.factorymethod.ingredients.meat;

public class SlicedPepperoni extends Pepperoni {
  @Override
  public String getTypeOfPepperoni() {
    return "sliced pepperoni, very hot type";
  }

  @Override
  public String getIngredientName() {
    return this.getClass().getName();
  }
}
