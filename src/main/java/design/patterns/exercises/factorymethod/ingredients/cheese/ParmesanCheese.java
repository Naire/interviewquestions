package design.patterns.exercises.factorymethod.ingredients.cheese;

public class ParmesanCheese extends Cheese {
  @Override
  public String getKindOfCheese() {
    return "parmesan";
  }

  @Override
  public String getIngredientName() {
    return this.getClass().getName();
  }
}
