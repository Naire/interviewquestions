package design.patterns.exercises.factorymethod.ingredients.meat;

import design.patterns.exercises.factorymethod.ingredients.Ingredient;

public abstract class Clams implements Ingredient {
  abstract String getDescription();

  @Override
  public String toString() {
    return String.format("Clams{%s}", getDescription());
  }
}
