package design.patterns.exercises.factorymethod.ingredients.meat;

public class FrozenClams extends Clams {
  @Override
  public String getDescription() {
    return "those clams were frozen but are still delicious";
  }

  @Override
  public String getIngredientName() {
    return this.getClass().getName();
  }
}
