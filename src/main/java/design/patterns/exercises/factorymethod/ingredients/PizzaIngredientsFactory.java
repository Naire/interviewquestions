package design.patterns.exercises.factorymethod.ingredients;

import java.util.Collection;
import design.patterns.exercises.factorymethod.ingredients.cheese.Cheese;
import design.patterns.exercises.factorymethod.ingredients.dough.Dough;
import design.patterns.exercises.factorymethod.ingredients.meat.Clams;
import design.patterns.exercises.factorymethod.ingredients.meat.Pepperoni;
import design.patterns.exercises.factorymethod.ingredients.sauce.Sauce;
import design.patterns.exercises.factorymethod.ingredients.veggie.Veggie;

public interface PizzaIngredientsFactory {
  Dough createDough();

  Sauce createSauce();

  Cheese createCheese();

  Collection<Veggie> createVeggies(Integer numberOfVeggies);

  Pepperoni createPepperoni();

  Clams createClam();
}
