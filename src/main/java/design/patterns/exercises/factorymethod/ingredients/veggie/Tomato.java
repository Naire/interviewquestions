package design.patterns.exercises.factorymethod.ingredients.veggie;

public class Tomato extends Veggie {
  @Override
  public String getVegetableType() {
    return "red tomato";
  }

  @Override
  public String getIngredientName() {
    return this.getClass().getName();
  }
}
