package design.patterns.exercises.factorymethod.ingredients.cheese;

public class MozzarellaCheese extends Cheese {
  @Override
  public String getKindOfCheese() {
    return "mozzarella";
  }

  @Override
  public String getIngredientName() {
    return this.getClass().getName();
  }
}
