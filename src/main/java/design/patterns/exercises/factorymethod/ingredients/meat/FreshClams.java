package design.patterns.exercises.factorymethod.ingredients.meat;

public class FreshClams extends Clams {
  @Override
  public String getDescription() {
    return "freshly caught clams";
  }

  @Override
  public String getIngredientName() {
    return this.getClass().getName();
  }
}
