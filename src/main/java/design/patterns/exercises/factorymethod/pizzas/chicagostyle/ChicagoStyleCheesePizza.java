package design.patterns.exercises.factorymethod.pizzas.chicagostyle;

import java.util.Arrays;
import design.patterns.exercises.factorymethod.ingredients.PizzaIngredientsFactory;
import design.patterns.exercises.factorymethod.pizzas.Pizza;

public class ChicagoStyleCheesePizza extends Pizza {
  private static final int numberOfSlices = 8;

  public ChicagoStyleCheesePizza(final PizzaIngredientsFactory pizzaIngredientsFactory) {
    super(pizzaIngredientsFactory);
    setName("Chicago Style Cheesy Cheese Pizza");
  }

  @Override
  public Pizza prepare() {
    System.out.printf("Preparing %s%n", getName());
    setDough(factory.createDough());
    setSauce(factory.createSauce());
    setCheese(factory.createCheese());
    addIngredients(Arrays.asList(getCheese(), getCheese()));
    return this;
  }

  @Override
  public Pizza bake() {
    System.out.println("Baking in 300C for half an hour");
    setBaked(true);
    return this;
  }

  @Override
  public Pizza cut() {
    setNumberOfSlices(numberOfSlices);
    System.out.printf("Cutting into %s squares for optimal eating experience :)%n", numberOfSlices);
    return this;
  }

  @Override
  public Pizza box() {
    setBox("Chicago style blue box");
    System.out.printf("Packing into %s%n", getBox());
    return this;
  }
}
