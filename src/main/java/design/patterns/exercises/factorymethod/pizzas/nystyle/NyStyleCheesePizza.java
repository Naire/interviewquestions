package design.patterns.exercises.factorymethod.pizzas.nystyle;

import design.patterns.exercises.factorymethod.ingredients.PizzaIngredientsFactory;
import design.patterns.exercises.factorymethod.pizzas.Pizza;

public class NyStyleCheesePizza extends Pizza {
  private static final int numberOfPieces = 8;

  public NyStyleCheesePizza(final PizzaIngredientsFactory factory) {
    super(factory);
    setName("New York Style Cheesy goodness");
  }

  @Override
  public Pizza prepare() {
    System.out.printf("Preparing %s%n", getName());
    setDough(factory.createDough());
    setSauce(factory.createSauce());
    setCheese(factory.createCheese());
    addIngredient(getCheese());
    return this;
  }

  @Override
  public Pizza bake() {
    System.out.printf("Baking in 250 C%n");
    setBaked(true);
    return this;
  }

  @Override
  public Pizza cut() {
    setNumberOfSlices(numberOfPieces);
    System.out.printf("Cutting into %d pieces%n", numberOfPieces);
    return this;
  }

  @Override
  public Pizza box() {
    System.out.printf("Packed in %s%n", getBox());
    return this;
  }
}
