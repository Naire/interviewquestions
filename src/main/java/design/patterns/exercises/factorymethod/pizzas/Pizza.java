package design.patterns.exercises.factorymethod.pizzas;

//import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import design.patterns.exercises.factorymethod.ingredients.Ingredient;
import design.patterns.exercises.factorymethod.ingredients.PizzaIngredientsFactory;
import design.patterns.exercises.factorymethod.ingredients.cheese.Cheese;
import design.patterns.exercises.factorymethod.ingredients.dough.Dough;
import design.patterns.exercises.factorymethod.ingredients.meat.Clams;
import design.patterns.exercises.factorymethod.ingredients.meat.Pepperoni;
import design.patterns.exercises.factorymethod.ingredients.sauce.Sauce;
import design.patterns.exercises.factorymethod.ingredients.veggie.Veggie;

public abstract class Pizza {
  protected final PizzaIngredientsFactory factory;
  private final ArrayList<Ingredient> ingredients;
  private String name = "Unnamed";
  private Dough dough;
  private Sauce sauce;
  private Cheese cheese;
  private Collection<Veggie> veggies;
  private Pepperoni pepperoni;
  private Clams clams;
  private String box = "standard red box";
  private boolean baked = false;
  private int numberOfSlices = 1;

  public Pizza(final PizzaIngredientsFactory factory) {
    ingredients = new ArrayList<>();
    this.factory = factory;
  }

  /**
   * Note that NONE of the abstract methods are allowed to return null
   */
 // @NotNull
  public abstract Pizza prepare();

//  @NotNull
  public abstract Pizza bake();

//  @NotNull
  public abstract Pizza cut();

//  @NotNull
  public abstract Pizza box();

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Dough getDough() {
    return dough;
  }

  public void setDough(Dough dough) {
    this.dough = dough;
    addIngredient(dough);
  }

  public Sauce getSauce() {
    return sauce;
  }

  public void setSauce(Sauce sauce) {
    this.sauce = sauce;
    addIngredient(sauce);
  }

  public Cheese getCheese() {
    return cheese;
  }

  public void setCheese(Cheese cheese) {
    this.cheese = cheese;
    addIngredient(cheese);
  }

  public Collection<Veggie> getVeggies() {
    return veggies;
  }

  public void setVeggies(Collection<Veggie> veggies) {
    this.veggies = veggies;
    addIngredients(veggies);
  }

  public Pepperoni getPepperoni() {
    return pepperoni;
  }

  public void setPepperoni(Pepperoni pepperoni) {
    this.pepperoni = pepperoni;
    addIngredient(pepperoni);
  }

  public Clams getClams() {
    return clams;
  }

  public void setClams(Clams clams) {
    this.clams = clams;
    addIngredient(clams);
  }

  public String getBox() {
    return box;
  }

  public void setBox(String box) {
    this.box = box;
  }

  public boolean isBaked() {
    return baked;
  }

  public void setBaked(boolean baked) {
    this.baked = baked;
  }

  protected ArrayList<Ingredient> getIngredients() {
    return ingredients;
  }

  public int getNumberOfSlices() {
    return numberOfSlices;
  }

  protected void setNumberOfSlices(int numberOfSlices) {
    this.numberOfSlices = numberOfSlices;
  }

  protected void addIngredient(final Ingredient ingredient) {
    ingredients.add(ingredient);
    System.out.printf("Added %s%n", ingredient);
  }

  protected void addIngredients(Collection<? extends Ingredient> extraIngredients) {
    if (extraIngredients != null) {
      ingredients.addAll(extraIngredients);
      System.out.printf("Added %s%n", extraIngredients);
    }
  }

  @Override
  public String toString() {
    return "Pizza{" +
        "factory=" + factory +
        ", ingredients=" + ingredients +
        ", name='" + name + '\'' +
        ", dough=" + dough +
        ", sauce=" + sauce +
        ", cheese=" + cheese +
        ", veggies=" + veggies +
        ", pepperoni=" + pepperoni +
        ", clams=" + clams +
        ", box='" + box + '\'' +
        ", baked=" + baked +
        ", numberOfSlices=" + numberOfSlices +
        '}';
  }
}
