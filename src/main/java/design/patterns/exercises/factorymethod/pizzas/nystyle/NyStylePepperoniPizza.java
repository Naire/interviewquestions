package design.patterns.exercises.factorymethod.pizzas.nystyle;

import design.patterns.exercises.factorymethod.ingredients.PizzaIngredientsFactory;
import design.patterns.exercises.factorymethod.pizzas.Pizza;

public class NyStylePepperoniPizza extends Pizza {
  private static final int numberOfPieces = 15;

  public NyStylePepperoniPizza(final PizzaIngredientsFactory factory) {
    super(factory);
    setName("New York Style Pepperoni delicacy");
  }

  @Override
  public Pizza prepare() {
    System.out.printf("Preparing %s%n", getName());
    setDough(factory.createDough());
    setSauce(factory.createSauce());
    setPepperoni(factory.createPepperoni());
    setVeggies(factory.createVeggies(1));
    return this;
  }

  @Override
  public Pizza bake() {
    System.out.printf("Baking goodies in 300 C%n");
    setBaked(true);
    return this;
  }

  @Override
  public Pizza cut() {
    setNumberOfSlices(numberOfPieces);
    System.out.printf("Cutting into %d pieces%n", numberOfPieces);
    return this;
  }

  @Override
  public Pizza box() {
    System.out.println("No box, just eat it fresh :D");
    return this;
  }
}
