package design.patterns.exercises.factorymethod.pizzas.chicagostyle;

import design.patterns.exercises.factorymethod.ingredients.PizzaIngredientsFactory;
import design.patterns.exercises.factorymethod.pizzas.Pizza;

public class ChicagoStylePepperoniPizza extends Pizza {
  private static final int numberOfSquares = 10;

  public ChicagoStylePepperoniPizza(final PizzaIngredientsFactory factory) {
    super(factory);
    setName("Chicago style pepperoni pizza!");
  }

  @Override
  public Pizza prepare() {
    System.out.printf("Preparing %s%n", getName());
    setDough(factory.createDough());
    setSauce(factory.createSauce());
    setCheese(factory.createCheese());
    setPepperoni(factory.createPepperoni());
    return this;
  }

  @Override
  public Pizza bake() {
    System.out.println("Bakes in 300 C");
    return this;
  }

  @Override
  public Pizza cut() {
    setNumberOfSlices(numberOfSquares);
    System.out.printf("Cutting into %s squares%n", numberOfSquares);
    return this;
  }

  @Override
  public Pizza box() {
    setBox("Chicago box with little pepperoni design");
    System.out.printf("Boxed in %s%n", getBox());
    return this;
  }
}
