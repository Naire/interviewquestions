package design.patterns.exercises.factorymethod;

import design.patterns.exercises.factorymethod.exceptions.PizzaTypeNotFoundException;
import design.patterns.exercises.factorymethod.stores.ChicagoStylePizzaStore;
import design.patterns.exercises.factorymethod.stores.NYStylePizzaStore;
import design.patterns.exercises.factorymethod.stores.PizzaStores;

public class PizzeriaUSAStyle {

  /**
   * Factory method pattern AND Abstract Factory. All factories encapsulate object creation.
   *
   * Factory Method relies on inheritance: object creation is delegated to subclasses, which
   * implements the factory method to create objects.
   *
   * Abstract Factory relies on object composition: object creation is implemented in methods
   * exposed in the factory interface.
   *
   * The Abstract Factory Pattern provides an interface for creating families of related or
   * dependent objects without specifying their concrete classes.
   *
   * Often the methods of an Abstract Factory are implemented as factory methods.
   */

  public static void main(String[] args) {
    System.out.println("PizzeriaUSAStyle");
    PizzaStores store = new NYStylePizzaStore();
    System.out.println("Menu: " + ((NYStylePizzaStore) store).getListOfPossiblePizzas());
    try {
      store.orderPizza("NYSTYLECHEESEPIZZA");
      store.orderPizza("NYSTYLEPEPPERONIPIZZA");
    } catch (PizzaTypeNotFoundException e) {
      System.out.println(e.getMessage());
    }
    System.out.println("Chicago pizzeria");
    store = new ChicagoStylePizzaStore();
    System.out.println("Menu: " + ((ChicagoStylePizzaStore) store).getListOfPossiblePizzas());
    try {
      store.orderPizza("CHICAGOSTYLECHEESEPIZZA");
    } catch (PizzaTypeNotFoundException e) {
      System.out.println(e.getMessage());
    }
  }

}
