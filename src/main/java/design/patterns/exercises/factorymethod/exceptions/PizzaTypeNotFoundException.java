package design.patterns.exercises.factorymethod.exceptions;

public class PizzaTypeNotFoundException extends Exception {
  public PizzaTypeNotFoundException(String message) {
    super(message);
  }
}
