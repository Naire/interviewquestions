package design.patterns.exercises.facade;

/**
 * Facade pattern.
 *
 * The Facade Pattern provides a unified interface to a set of interfaces in a subsystem. Facade
 * defines a higher-level interface that makes the subsystem easier to use. A facade not only
 * simplifies an interface, it decouples a client from a subsystem of components.
 */
public class HomeMovieTheater {
  public static void main(String[] args) {
    String movie = "The lion king";
    System.out.printf("This is your home theater. Movie %s is about to start.%n", movie);
    HomeTheaterFacade facade = new HomeTheaterFacade();
    facade.watchMovie(movie);
    System.out.println("*******************************");
    facade.endWatching();
  }
}
