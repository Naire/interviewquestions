package design.patterns.exercises.facade.devices;

public class DvdPlayer implements Player {
  private boolean isPlaying = false;

  @Override
  public void play(String movie) {
    System.out.printf("Searching library for movie: %s%n", movie);
    System.out.println("Playing movie...");
    isPlaying = true;
  }

  @Override
  public void on() {
    if (isPlaying) {
      System.out.println("DVD already on");
    } else {
      System.out.println("DVD player on");
    }
  }

  @Override
  public void off() {
    if (isPlaying) {
      System.out.println("DVD player off");
    } else {
      System.out.println("DVD already off");
    }
  }
}
