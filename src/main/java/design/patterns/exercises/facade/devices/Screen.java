package design.patterns.exercises.facade.devices;

public class Screen {
  public void down() {
    System.out.println("Screen's coming down");
  }

  public void up() {
    System.out.println("Screen's coming up!");
  }
}
