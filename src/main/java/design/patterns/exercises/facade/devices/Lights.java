package design.patterns.exercises.facade.devices;

public class Lights {
  private static final int MAX_LEVEL = 10;
  private int level = MAX_LEVEL;

  public void dim(int howMuch) {
    System.out.printf("Previous light level: %d%n", level);
    level = level - howMuch;
    System.out.printf("Dimming lights to %d level.%n", level);
  }

  public void on() {
    System.out.println("Lights are on");
    level = MAX_LEVEL;
  }
}
