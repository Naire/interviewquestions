package design.patterns.exercises.facade.devices;

public class Popcorn {
  public void on() {
    System.out.println("Turning the popcorn machine on...");
  }

  public void pop() {
    System.out.println("Pop pop pop!");
  }

  public void off() {
    System.out.println("Turning the popcorn machine off...");
  }
}
