package design.patterns.exercises.facade.devices;

public class Amplifier {
  private Player player;
  private int curentVolume = 0;

  public void on() {
    System.out.println("Amplifier is on");
  }

  public void setDvd(Player player) {
    this.player = player;
  }

  public void setSurroundSound() {
    System.out.println("Surround on");
  }

  public void setVolume(int volume) {
    curentVolume = volume;
  }

  public void off() {
    System.out.println("Amplifier off");
  }
}
