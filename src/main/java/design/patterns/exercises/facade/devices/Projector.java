package design.patterns.exercises.facade.devices;

public class Projector {
  public void on() {
    System.out.println("Projector is starting up.");
  }

  public void wideScreenMode() {
    System.out.println("Wide screen mode on");
  }

  public void normalMode() {
    System.out.println("Default mode on");
  }

  public void off() {
    System.out.println("Projector off");
  }
}
