package design.patterns.exercises.facade.devices;

public class CdPlayer implements Player {
  @Override
  public void play(String movie) {
    System.out.printf("Playing movie: %s%n", movie);
  }

  @Override
  public void on() {
    System.out.println("CD player on");
  }

  @Override
  public void off() {
    System.out.println("CD player off");
  }
}
