package design.patterns.exercises.facade.devices;

public interface Player {
  void play(String movie);

  void on();

  void off();
}
