package design.patterns.exercises.facade;

import design.patterns.exercises.facade.devices.Amplifier;
import design.patterns.exercises.facade.devices.CdPlayer;
import design.patterns.exercises.facade.devices.DvdPlayer;
import design.patterns.exercises.facade.devices.Lights;
import design.patterns.exercises.facade.devices.Player;
import design.patterns.exercises.facade.devices.Popcorn;
import design.patterns.exercises.facade.devices.Projector;
import design.patterns.exercises.facade.devices.Screen;
import design.patterns.exercises.facade.devices.Tuner;

public class HomeTheaterFacade {
  private Amplifier amplifier;
  private Tuner tuner;
  private DvdPlayer dvdPlayer;
  private CdPlayer cdPlayer;
  private Projector projector;
  private Lights lights;
  private Screen screen;
  private Popcorn popcorn;

  public HomeTheaterFacade() {
    System.out.println("Default devices initializing...");
    this.amplifier = new Amplifier();
    this.tuner = new Tuner();
    this.dvdPlayer = new DvdPlayer();
    this.cdPlayer = new CdPlayer();
    this.projector = new Projector();
    this.lights = new Lights();
    this.screen = new Screen();
    this.popcorn = new Popcorn();
  }

  public HomeTheaterFacade(Amplifier amplifier, Tuner tuner,
                           DvdPlayer dvdPlayer,
                           CdPlayer cdPlayer,
                           Projector projector,
                           Lights lights, Screen screen,
                           Popcorn popcorn) {
    this.amplifier = amplifier;
    this.tuner = tuner;
    this.dvdPlayer = dvdPlayer;
    this.cdPlayer = cdPlayer;
    this.projector = projector;
    this.lights = lights;
    this.screen = screen;
    this.popcorn = popcorn;
  }

  public void watchMovie(final String movieTitle) {
    System.out.printf("Preparing to watch movie: %s%n", movieTitle);
    makePopcorn();
    lights.dim(10);
    screen.down();
    prepareProjector();
    prepareAmplifier(dvdPlayer);
    playMovie(movieTitle);
  }

  public void endWatching() {
    System.out.println("Shutting down the theater...");
    lights.on();
    stopCurrentMovie();
    amplifier.off();
    shutDownProjector();
    screen.up();
  }

  private void shutDownProjector() {
    projector.normalMode();
    projector.off();
  }

  private void stopCurrentMovie() {
    dvdPlayer.off();
    cdPlayer.off();
  }

  private void makePopcorn() {
    popcorn.on();
    popcorn.pop();
    popcorn.off();
  }

  private void prepareProjector() {
    projector.on();
    projector.wideScreenMode();
  }

  private void prepareAmplifier(final Player player) {
    amplifier.on();
    amplifier.setDvd(player);
    amplifier.setSurroundSound();
    amplifier.setVolume(5);
  }

  private void playMovie(final String movieTitle) {
    dvdPlayer.on();
    dvdPlayer.play(movieTitle);
  }
}
