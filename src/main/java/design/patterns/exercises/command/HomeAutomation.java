package design.patterns.exercises.command;

import design.patterns.exercises.command.commands.off.GarageDoorCloseCommand;
import design.patterns.exercises.command.commands.off.LightOffCommand;
import design.patterns.exercises.command.commands.on.GarageDoorOpenCommand;
import design.patterns.exercises.command.commands.on.LightOnCommand;
import design.patterns.exercises.command.remotes.RemoteControl;
import design.patterns.exercises.command.vendors.GarageDoor;
import design.patterns.exercises.command.vendors.GarageDoorImpl;
import design.patterns.exercises.command.vendors.Light;
import design.patterns.exercises.command.vendors.LivingRoomLight;
import design.patterns.exercises.command.withjdk8.Remote8;

/**
 * The Command Pattern encapsulates a request as an object, thereby letting you parameterize other
 * objects with different requests, queue or log requests, and support undoable operations.
 */
public class HomeAutomation {
  public static void main(String[] args) {
    System.out.println("Home automation");
   /* SimpleRemoteControl remote = new SimpleRemoteControl();
    Light light = new LivingRoomLight();
    LightOnCommand lightOn = new LightOnCommand(light);
    remote.setCommand(lightOn);
    remote.buttonWasPressed();

    System.out.println();*/
    RemoteControl control = new RemoteControl(5);
    Light light = new LivingRoomLight();
    LightOnCommand lightOn = new LightOnCommand(light);
    LightOffCommand lightOff = new LightOffCommand(light);
    control.setCommand(0, lightOn, lightOff);
    GarageDoor garageDoor = new GarageDoorImpl();
    GarageDoorCloseCommand closeGarage = new GarageDoorCloseCommand(garageDoor);
    GarageDoorOpenCommand openGarage = new GarageDoorOpenCommand(garageDoor);
    control.setCommand(1, openGarage, closeGarage);

    control.onButtonWasPushed(0);
    control.onButtonWasPushed(1);
    control.undoWasPushed();

    System.out.println(control);

    System.out.println("**********");

    /*Example of using streams - when the Command interface is functional (has only one method)
     * then we can use lambdas instead of creating a lot of small ON/OFF command classes*/
    Remote8 remote = new Remote8(5);
    remote.setCommand(0, light::on, light::off);
  }
}
