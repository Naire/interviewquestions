package design.patterns.exercises.command.remotes;

import design.patterns.exercises.command.commands.Command;

/**
 * This interface models a remote controller that has buttons: on/off and slots with corresponding
 * devices programmed.
 */
public interface Remote {
  void setCommand(int whichSlot, Command onCommand, Command offCommand);

  void onButtonWasPushed(int whichSlot);

  void offButtonWasPushed(int whichSlot);

  void undoWasPushed();
}
