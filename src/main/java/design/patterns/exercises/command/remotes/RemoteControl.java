package design.patterns.exercises.command.remotes;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import design.patterns.exercises.command.commands.Command;
import design.patterns.exercises.command.commands.NoCommand;

public class RemoteControl implements Remote {
  private final List<Command> onCommands;
  private final List<Command> offCommands;
  private final int numberOfSlots;
  private Command undoCommand; //only last action can be undone


  public RemoteControl(int numberOfSlots) {
    this.numberOfSlots = numberOfSlots;
    onCommands = new ArrayList<>(numberOfSlots);
    offCommands = new ArrayList<>(numberOfSlots);
    assignNoCommandDefault();
  }

  private void assignNoCommandDefault() {
    final NoCommand noCommand = new NoCommand();
    for (int i = 0; i < numberOfSlots; i++) {
      onCommands.add(noCommand);
      offCommands.add(noCommand);
    }
    undoCommand = noCommand;
  }

  @Override
  public void setCommand(int whichSlot, Command onCommand, Command offCommand) {
    onCommands.add(whichSlot, onCommand);
    offCommands.add(whichSlot, offCommand);
  }

  @Override
  public void onButtonWasPushed(int whichSlot) {
    if (slotExists(whichSlot)) {
      final Command onCommand = onCommands.get(whichSlot);
      onCommand.execute();
      undoCommand = onCommand;
    } else {
      //this here should be an exception
      System.out.println("Invalid slot number");
    }
  }

  @Override
  public void offButtonWasPushed(int whichSlot) {
    if (slotExists(whichSlot)) {
      final Command offCommand = offCommands.get(whichSlot);
      offCommand.execute();
      undoCommand = offCommand;
    } else {
      //this here should be an exception
      System.out.println("Invalid slot number");
    }

  }

  @Override
  public void undoWasPushed() {
    System.out.println("Undoing last action...");
    undoCommand.undo();
  }

  private boolean slotExists(int whichSlot) {
    return (0 <= whichSlot) && (whichSlot < numberOfSlots);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("RemoteControl{");
    sb.append("onCommands=")
        .append(
            onCommands.stream().map(command -> command.getClass().getSimpleName() + ", ").collect(
                Collectors.joining()));
    sb.append("offCommands=")
        .append(
            offCommands.stream().map(command -> command.getClass().getSimpleName() + ", ").collect(
                Collectors.joining()));
    sb.append("numberOfSlots=").append(numberOfSlots);
    sb.append('}');
    return sb.toString();
  }
}
