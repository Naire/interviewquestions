package design.patterns.exercises.command.remotes;

import design.patterns.exercises.command.commands.Command;

public class SimpleRemoteControl {
  private Command slot;

  public void setCommand(Command command) {
    this.slot = command;
  }

  public void buttonWasPressed() {
    slot.execute();
  }
}
