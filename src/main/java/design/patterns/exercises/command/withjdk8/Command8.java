package design.patterns.exercises.command.withjdk8;

public interface Command8 {
  void execute();
}
