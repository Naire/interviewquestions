package design.patterns.exercises.command.withjdk8;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Remote8 {
  private final List<Command8> onCommands;
  private final List<Command8> offCommands;
  private final int numberOfSlots;

  public Remote8(int numberOfSlots) {
    this.numberOfSlots = numberOfSlots;
    onCommands = new ArrayList<>(numberOfSlots);
    offCommands = new ArrayList<>(numberOfSlots);
    final Stream<Command8> noCommandStream =
        IntStream.range(0, numberOfSlots).mapToObj(i -> () -> {
        });
    noCommandStream.forEach(noCommand -> {
      onCommands.add(noCommand);
      offCommands.add(noCommand);
    });
  }

  //same code as in RemoteControl class

  public void setCommand(int whichSlot, Command8 onCommand, Command8 offCommand) {
    onCommands.add(whichSlot, onCommand);
    offCommands.add(whichSlot, offCommand);
  }


  public void onButtonWasPushed(int whichSlot) {
    if (slotExists(whichSlot)) {
      final Command8 onCommand = onCommands.get(whichSlot);
      onCommand.execute();
    } else {
      //this here should be an exception
      System.out.println("Invalid slot number");
    }
  }


  public void offButtonWasPushed(int whichSlot) {
    if (slotExists(whichSlot)) {
      final Command8 offCommand = offCommands.get(whichSlot);
      offCommand.execute();
    } else {
      //this here should be an exception
      System.out.println("Invalid slot number");
    }

  }


  public void undoWasPushed() {
    System.out.println("No undo here");
  }

  private boolean slotExists(int whichSlot) {
    return (0 <= whichSlot) && (whichSlot < numberOfSlots);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("RemoteControl{");
    sb.append("onCommands=")
        .append(
            onCommands.stream().map(command -> command.getClass().getSimpleName() + ", ").collect(
                Collectors.joining()));
    sb.append("offCommands=")
        .append(
            offCommands.stream().map(command -> command.getClass().getSimpleName() + ", ").collect(
                Collectors.joining()));
    sb.append("numberOfSlots=").append(numberOfSlots);
    sb.append('}');
    return sb.toString();
  }
}
