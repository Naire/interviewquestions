package design.patterns.exercises.command.commands.off;

import design.patterns.exercises.command.commands.Command;
import design.patterns.exercises.command.commands.on.GarageDoorOpenCommand;
import design.patterns.exercises.command.vendors.GarageDoor;

public class GarageDoorCloseCommand implements Command {
  private GarageDoor garageDoor;

  public GarageDoorCloseCommand(GarageDoor garageDoor) {
    this.garageDoor = garageDoor;
  }

  @Override
  public void execute() {
    garageDoor.lightOff();
    garageDoor.down();
    garageDoor.stop();
  }

  @Override
  public void undo() {
    new GarageDoorOpenCommand(garageDoor).execute();
  }
}
