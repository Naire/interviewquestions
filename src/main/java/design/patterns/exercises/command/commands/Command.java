package design.patterns.exercises.command.commands;

public interface Command {
  void execute();

  void undo();
}
