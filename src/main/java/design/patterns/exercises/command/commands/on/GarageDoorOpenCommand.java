package design.patterns.exercises.command.commands.on;

import design.patterns.exercises.command.commands.Command;
import design.patterns.exercises.command.commands.off.GarageDoorCloseCommand;
import design.patterns.exercises.command.vendors.GarageDoor;

public class GarageDoorOpenCommand implements Command {
  private GarageDoor garageDoor;

  public GarageDoorOpenCommand(GarageDoor garageDoor) {
    this.garageDoor = garageDoor;
  }

  @Override
  public void execute() {
    garageDoor.up();
    garageDoor.stop();
    garageDoor.lightOn();
  }

  @Override
  public void undo() {
    new GarageDoorCloseCommand(garageDoor).execute();
  }
}
