package design.patterns.exercises.command.commands.off;

import design.patterns.exercises.command.commands.Command;
import design.patterns.exercises.command.vendors.Light;

public class LightOffCommand implements Command {
  private Light light;

  public LightOffCommand(Light light) {
    this.light = light;
  }

  @Override
  public void execute() {
    light.off();
  }

  @Override
  public void undo() {
    light.on();
  }
}
