package design.patterns.exercises.command.commands.on;

import design.patterns.exercises.command.commands.Command;
import design.patterns.exercises.command.vendors.Light;

public class LightOnCommand implements Command {
  private Light light;

  public LightOnCommand(Light light) {
    this.light = light;
  }

  @Override
  public void execute() {
    light.on();
  }

  @Override
  public void undo() {
    light.off();
  }
}
