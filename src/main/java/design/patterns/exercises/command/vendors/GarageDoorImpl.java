package design.patterns.exercises.command.vendors;

public class GarageDoorImpl implements GarageDoor {
  @Override
  public void up() {
    System.out.println("Garage door is opening");
  }

  @Override
  public void down() {
    System.out.println("Going down!");
  }

  @Override
  public void stop() {
    System.out.println("Stopped");
  }

  @Override
  public void lightOn() {
    System.out.println("Light is on in the garage");
  }

  @Override
  public void lightOff() {
    System.out.println("Light is off in the garage");
  }
}
