package design.patterns.exercises.command.vendors;

public class LivingRoomLight implements Light {
  @Override
  public void on() {
    System.out.println("Light is on!");
  }

  @Override
  public void off() {
    System.out.println("Totally dark here...");
  }
}
