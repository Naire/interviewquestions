package design.patterns.exercises.command.vendors;

public interface GarageDoor {
  void up();

  void down();

  void stop();

  void lightOn();

  void lightOff();
}
