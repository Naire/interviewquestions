package design.patterns.exercises.command.vendors;

public interface Light {
  void on();

  void off();
}
