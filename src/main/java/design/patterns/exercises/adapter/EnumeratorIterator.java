package design.patterns.exercises.adapter;

import java.util.Enumeration;
import java.util.Iterator;

/**
 * The Adapter Pattern converts the interface of a class into another interface the clients expect.
 * Adapter lets classes work together that couldn’t otherwise because of incompatible interfaces.
 *
 * The intent of the Adapter Pattern is to alter an interface so that it matches one a client is
 * expecting. The intent of the Facade Pattern is to provide a simplified interface to a subsystem.
 */
public class EnumeratorIterator implements Iterator {
  private final Enumeration<?> enumeration;

  public EnumeratorIterator(Enumeration<?> enumeration) {
    this.enumeration = enumeration;
  }

  @Override
  public boolean hasNext() {
    return enumeration.hasMoreElements();
  }

  @Override
  public Object next() {
    return enumeration.nextElement();
  }
}
