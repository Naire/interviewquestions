package design.patterns.exercises.decorator.beverages;

public abstract class Beverage {

  String description = "Unknown beverage";
  Size size = Size.TALL;

  public String getDescription() {
    return description;
  }

  public abstract double cost(); //double is not a good data type for money but for the sake of this example I'll use it

  public Size getSize() {
    return this.size;
  }

  public void setSize(Size size) {
    this.size = size;
  }

  public enum Size {TALL, GRANDE, VENTI}
}
