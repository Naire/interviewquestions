package design.patterns.exercises.decorator.beverages;

public class Espresso extends Beverage {

  private static double price = 1.99;
  private static String espressoDesc = "Espresso";

  public Espresso() {
    description = Espresso.espressoDesc;
  }

  @Override
  public double cost() {
    return price;
  }
}
