package design.patterns.exercises.decorator.beverages;

public class Decaf extends Beverage {

  private static double price = 1.05;
  private static String decafDesc = "Decaf Coffee";

  public Decaf() {
    description = decafDesc;
  }

  @Override
  public double cost() {
    return price;
  }
}
