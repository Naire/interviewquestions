package design.patterns.exercises.decorator.beverages;

public class HouseBlend extends Beverage {

  private static double price = 0.89;
  private static String houseBlendDesc = "House Blend Coffee";

  public HouseBlend() {
    description = houseBlendDesc;
  }

  @Override
  public double cost() {
    return price;
  }
}
