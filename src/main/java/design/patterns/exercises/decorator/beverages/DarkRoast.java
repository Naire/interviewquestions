package design.patterns.exercises.decorator.beverages;

public class DarkRoast extends Beverage {

  private static double price = 0.99;
  private static String darkRoastDesc = "Dark Roast Coffee";

  public DarkRoast() {
    description = darkRoastDesc;
  }

  @Override
  public double cost() {
    return price;
  }
}
