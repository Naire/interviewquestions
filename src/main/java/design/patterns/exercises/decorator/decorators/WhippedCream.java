package design.patterns.exercises.decorator.decorators;

import design.patterns.exercises.decorator.beverages.Beverage;

public class WhippedCream extends CondimentDecorator {

  private static String whipDesc = "whipped cream";

  private Beverage beverage;

  public WhippedCream(Beverage beverage) {
    this.beverage = beverage;
    tallPrice = 0.30;
    grandePrice = 0.40;
    vendiPrice = 0.50;
  }

  @Override
  public String getDescription() {
    return String.format("%s, %s", beverage.getDescription(), whipDesc);
  }

  @Override
  public double cost() {
    return beverage.cost() + calculatePrice(beverage.getSize());
  }
}
