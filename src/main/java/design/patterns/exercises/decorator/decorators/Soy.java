package design.patterns.exercises.decorator.decorators;

import design.patterns.exercises.decorator.beverages.Beverage;

public class Soy extends CondimentDecorator {

  private static String soyDesc = "soy milk";

  private Beverage beverage;

  public Soy(Beverage beverage) {
    this.beverage = beverage;
    tallPrice = 0.10;
    grandePrice = 0.15;
    vendiPrice = 0.20;
  }

  @Override
  public String getDescription() {
    return String.format("%s, %s", beverage.getDescription(), soyDesc);
  }

  @Override
  public double cost() {
    return beverage.cost() + calculatePrice(beverage.getSize());
  }
}
