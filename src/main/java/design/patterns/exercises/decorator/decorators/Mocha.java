package design.patterns.exercises.decorator.decorators;

import design.patterns.exercises.decorator.beverages.Beverage;

public class Mocha extends CondimentDecorator {

  private static String mochaDesc = "mocha";

  private Beverage beverage;

  public Mocha(Beverage beverage) {
    this.beverage = beverage;
    tallPrice = 0.15;
    grandePrice = 0.20;
    vendiPrice = 0.25;
  }

  @Override
  public String getDescription() {
    return String.format("%s, %s", beverage.getDescription(), mochaDesc);
  }

  @Override
  public double cost() {
    return beverage.cost() + calculatePrice(beverage.getSize());
  }
}
