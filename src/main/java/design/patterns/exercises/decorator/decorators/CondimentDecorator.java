package design.patterns.exercises.decorator.decorators;

import design.patterns.exercises.decorator.beverages.Beverage;

public abstract class CondimentDecorator extends Beverage {

  double tallPrice = 0.10;
  double grandePrice = 0.15;
  double vendiPrice = 0.20;

  double calculatePrice(Size size) {
    switch (size) {
      case TALL:
        return tallPrice;
      case GRANDE:
        return grandePrice;
      case VENTI:
        return vendiPrice;
    }
    return tallPrice;
  }

  public abstract String getDescription();
}
