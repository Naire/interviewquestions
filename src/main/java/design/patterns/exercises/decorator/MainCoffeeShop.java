package design.patterns.exercises.decorator;

import design.patterns.exercises.decorator.beverages.Beverage;
import design.patterns.exercises.decorator.beverages.Beverage.Size;
import design.patterns.exercises.decorator.beverages.DarkRoast;
import design.patterns.exercises.decorator.beverages.Espresso;
import design.patterns.exercises.decorator.decorators.Mocha;
import design.patterns.exercises.decorator.decorators.WhippedCream;


/**
 * Decorator pattern.
 *
 * The Decorator Pattern attaches additional responsibilities to an object dynamically. Decorators
 * provide a flexible alternative to subclassing for extending functionality. Stays true to the
 * Open-Closed Principle.
 *
 * Usage: Java I/O libraries.
 */
public class MainCoffeeShop {

  public static void main(String[] args) {
    System.out.println("Welcome to the Coffee Shop :)");

    Beverage espresso = new Espresso();
    System.out.printf("Here is your %s $%s%n", espresso.getDescription(), espresso.cost());

    Beverage darkRoast = new DarkRoast();
    darkRoast.setSize(Size.GRANDE);
    darkRoast = new Mocha(darkRoast);
    darkRoast = new Mocha(darkRoast);
    darkRoast = new WhippedCream(darkRoast);

    System.out.printf("Here is your %s %s $%.2f%n", darkRoast.getSize(), darkRoast.getDescription(),
        darkRoast.cost());
  }
}
