package design.patterns.exercises.observer.javaAPIobserver.displays;

import java.util.Observable;
import java.util.Observer;
import design.patterns.exercises.observer.displays.DisplayElement;
import design.patterns.exercises.observer.javaAPIobserver.WeatherDataWithJavaAPI;

public class HeatIndexDisplay extends DisplayElement implements Observer {
  private Observable observable;
  private double heatIndex;

  public HeatIndexDisplay(Observable subject) {
    observable = subject;
    observable.addObserver(this);
  }

  @Override
  public void display() {
    System.out.printf("Heat index is: %s%n", heatIndex);
  }

  @Override
  public void update(Observable observable, Object arg) {
    if (observable instanceof WeatherDataWithJavaAPI) {
      WeatherDataWithJavaAPI weatherData = (WeatherDataWithJavaAPI) observable;
      heatIndex = computeHeatIndex(weatherData.getTemperature(), weatherData.getHumidity());
      display();
    }

  }

  /**
   * This is copied from https://www.wickedlysmart.com/headfirstdesignpatterns/heatindex.txt
   */
  private double computeHeatIndex(double temperature, double relativeHumidity) {
    return (
        (16.923 + (0.185212 * temperature) + (5.37941 * relativeHumidity) - (0.100254 * temperature
            * relativeHumidity) +
            (0.00941695 * (temperature * temperature)) + (0.00728898 * (relativeHumidity
            * relativeHumidity)) +
            (0.000345372 * (temperature * temperature * relativeHumidity)) - (0.000814971 * (
            temperature * relativeHumidity * relativeHumidity)) +
            (0.0000102102 * (temperature * temperature * relativeHumidity * relativeHumidity)) - (
            0.000038646 * (temperature * temperature * temperature)) + (0.0000291583 *
            (relativeHumidity * relativeHumidity * relativeHumidity)) + (0.00000142721 * (
            temperature * temperature * temperature * relativeHumidity)) +
            (0.000000197483 * (temperature * relativeHumidity * relativeHumidity
                * relativeHumidity)) - (0.0000000218429 * (temperature * temperature * temperature
            * relativeHumidity * relativeHumidity)) +
            0.000000000843296 * (temperature * temperature * relativeHumidity * relativeHumidity
                * relativeHumidity)) -
            (0.0000000000481975 * (temperature * temperature * temperature * relativeHumidity
                * relativeHumidity * relativeHumidity)));
  }
}
