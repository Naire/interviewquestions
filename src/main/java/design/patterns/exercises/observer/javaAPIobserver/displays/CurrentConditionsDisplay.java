package design.patterns.exercises.observer.javaAPIobserver.displays;

import java.util.Observable;
import java.util.Observer;
import design.patterns.exercises.observer.displays.DisplayElement;
import design.patterns.exercises.observer.javaAPIobserver.WeatherDataWithJavaAPI;

public class CurrentConditionsDisplay extends DisplayElement implements Observer {
  private Observable observable;

  public CurrentConditionsDisplay(Observable subject) {
    observable = subject;
    observable.addObserver(this);
  }

  @Override
  public void display() {
    System.out.println("Current condition display");
    System.out
        .printf("Current temperature %.2f C, humidity %.2f %n", getTemperature(), getHumidity());
  }

  @Override
  public void update(Observable observable, Object arg) {
    if (observable instanceof WeatherDataWithJavaAPI) {
      WeatherDataWithJavaAPI weatherData = (WeatherDataWithJavaAPI) observable;
      setTemperature(weatherData.getTemperature());
      setHumidity(weatherData.getHumidity());
      display();
    }
  }
}
