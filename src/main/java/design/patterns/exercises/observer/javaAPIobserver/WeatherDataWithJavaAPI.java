package design.patterns.exercises.observer.javaAPIobserver;

import java.util.Observable;
import java.util.Random;

/*This is a copy of WeatherData with commented out code that is no longer needed while using java.util.Observable*/
public class WeatherDataWithJavaAPI extends Observable {
  //private ArrayList<Observer> observers;
  private double temperature;
  private double humidity;
  private double pressure;

  public WeatherDataWithJavaAPI() {
    //observers = new ArrayList<>();
  }

  /*  @Override
    public void registerObserver(Observer observer) {
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(observer -> observer.update(temperature, humidity, pressure));

    }*/

  private static double random(double min, double max) {
    return min + (new Random(System.nanoTime()).nextDouble() * (max - min));
  }

  private static double gatherTemperatureData() {
    System.out.println("Getting current temperature from the station!");
    return random(-50, 50);
  }

  private static double gatherHumidityData() {
    System.out.println("Getting current humidity from the station!");
    return random(0, 100);
  }

  private static double gatherPressureData() {
    System.out.println("Getting current pressure from the station!");
    return random(1000, 5000);
  }

  public void gatherCurrentWeatherData() {
    temperature = gatherTemperatureData();
    humidity = gatherHumidityData();
    pressure = gatherPressureData();
    measurementsChanged();
  }

  public double getTemperature() {
    return temperature;
  }

  public double getHumidity() {
    return humidity;
  }

  public double getPressure() {
    return pressure;
  }

  private void measurementsChanged() {
    setChanged(); //sets a flag in the Observable class
    notifyObservers(); //no object passed mean it will be PULL not push
  }
}
