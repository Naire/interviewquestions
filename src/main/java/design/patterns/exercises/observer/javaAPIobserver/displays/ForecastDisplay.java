package design.patterns.exercises.observer.javaAPIobserver.displays;

import java.util.Observable;
import java.util.Observer;
import design.patterns.exercises.observer.displays.DisplayElement;
import design.patterns.exercises.observer.javaAPIobserver.WeatherDataWithJavaAPI;

public class ForecastDisplay extends DisplayElement implements Observer {
  private Observable observable;

  public ForecastDisplay(Observable subject) {
    observable = subject;
    observable.addObserver(this);
  }

  @Override
  public void display() {
    System.out.println("Forecast display");
    System.out.printf("Forecast: temp %.2f C, humidity: %.2f, pressure: %.2f%n", getTemperature(),
        getHumidity(), getPressure());
  }

  @Override
  public void update(Observable observable, Object arg) {
    if (observable instanceof WeatherDataWithJavaAPI) {
      WeatherDataWithJavaAPI weatherData = (WeatherDataWithJavaAPI) observable;
      setTemperature(weatherData.getTemperature());
      setHumidity(weatherData.getHumidity());
      setPressure(weatherData.getPressure());
      display();
    }

  }
}
