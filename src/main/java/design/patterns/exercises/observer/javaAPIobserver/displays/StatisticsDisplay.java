package design.patterns.exercises.observer.javaAPIobserver.displays;

import java.util.Observable;
import java.util.Observer;
import design.patterns.exercises.observer.displays.DisplayElement;
import design.patterns.exercises.observer.javaAPIobserver.WeatherDataWithJavaAPI;

public class StatisticsDisplay extends DisplayElement implements Observer {
  private Observable observable;

  public StatisticsDisplay(Observable subject) {
    observable = subject;
    observable.addObserver(this);
  }

  @Override
  public void display() {
    System.out.println("Statistics display");
    System.out.printf("Stats temperature %.2f C, humidity %.2f, pressure %.2f %n", getTemperature(),
        getHumidity(), getPressure());
  }


  @Override
  public void update(Observable observable, Object arg) {
    if (observable instanceof WeatherDataWithJavaAPI) {
      WeatherDataWithJavaAPI weatherdata = (WeatherDataWithJavaAPI) observable;
      setTemperature(weatherdata.getTemperature());
      setHumidity(weatherdata.getHumidity());
      setPressure(weatherdata.getPressure());
      display();
    }
  }
}
