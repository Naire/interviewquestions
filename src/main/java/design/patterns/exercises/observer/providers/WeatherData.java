package design.patterns.exercises.observer.providers;

import java.util.ArrayList;
import java.util.Random;
import design.patterns.exercises.observer.interfaces.Observer;
import design.patterns.exercises.observer.interfaces.Subject;

public class WeatherData implements Subject {
  private ArrayList<Observer> observers;
  private double temperature;
  private double humidity;
  private double pressure;

  public WeatherData() {
    observers = new ArrayList<>();
  }

  private static double random(double min, double max) {
    return min + (new Random(System.nanoTime()).nextDouble() * (max - min));
  }

  private static double getTemperature() {
    System.out.println("Getting current temperature from the station!");
    return random(-50, 50);
  }

  private static double getHumidity() {
    System.out.println("Getting current humidity from the station!");
    return random(0, 100);
  }

  private static double getPressure() {
    System.out.println("Getting current pressure from the station!");
    return random(1000, 5000);
  }

  @Override
  public void registerObserver(Observer observer) {
    if (!observers.contains(observer)) {
      observers.add(observer);
    }
  }

  @Override
  public void removeObserver(Observer observer) {
    observers.remove(observer);
  }

  @Override
  public void notifyObservers() {
    observers.forEach(observer -> observer.update(temperature, humidity, pressure));

  }

  public void gatherCurrentWeatherData() {
    temperature = getTemperature();
    humidity = getHumidity();
    pressure = getPressure();
    measurementsChanged();
  }

  private void measurementsChanged() {
    notifyObservers();
  }
}
