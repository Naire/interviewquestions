package design.patterns.exercises.observer;

import design.patterns.exercises.observer.displays.CurrentConditionsDisplay;
import design.patterns.exercises.observer.displays.DisplayElement;
import design.patterns.exercises.observer.displays.ForecastDisplay;
import design.patterns.exercises.observer.displays.HeatIndexDisplay;
import design.patterns.exercises.observer.displays.StatisticsDisplay;
import design.patterns.exercises.observer.javaAPIobserver.WeatherDataWithJavaAPI;
import design.patterns.exercises.observer.providers.WeatherData;

/**
 * Observer pattern.
 *
 * Defines a one-to-many dependency between objects so that when one object changes state, all its
 * dependents are notified and updated automatically.
 */
public class MainWeatherForecast {
  public static void main(String[] args) {
    System.out.println("Weather forecast observer pattern.");

    WeatherData weatherData = new WeatherData();

    DisplayElement currentDisplay = new CurrentConditionsDisplay(weatherData);
    DisplayElement forecastDisplay = new ForecastDisplay(weatherData);
    DisplayElement statsDisplay = new StatisticsDisplay(weatherData);
    DisplayElement heatIndex = new HeatIndexDisplay(weatherData);

    weatherData.gatherCurrentWeatherData();


/*------------------------------------------------//
    Using java api
    Problems:
    - Observable is a class, you have to subclass it. That means you can’t add on the Observable
    behavior to an existing class that already extends another superclass.
    - because there isn’t an Observable interface, you can’t even create your own implementation
    that plays well with Java’s built-in Observer API.
        Nor do you have the option of swapping out the java.util implementation for another
        (say, a new, multithreaded implementation)
    - The design violates a second design principle: favor composition over inheritance

//------------------------------------------------*/
    WeatherDataWithJavaAPI weatherDataWithJavaAPI = new WeatherDataWithJavaAPI();

    DisplayElement
        currentDisplayWithApi =
        new design.patterns.exercises.observer.javaAPIobserver.displays.CurrentConditionsDisplay(
            weatherDataWithJavaAPI);
    DisplayElement
        forecastDisplayWithApi =
        new design.patterns.exercises.observer.javaAPIobserver.displays.ForecastDisplay(
            weatherDataWithJavaAPI);
    DisplayElement
        statsDisplayWithApi =
        new design.patterns.exercises.observer.javaAPIobserver.displays.StatisticsDisplay(
            weatherDataWithJavaAPI);
    DisplayElement
        heatIndexWithApi =
        new design.patterns.exercises.observer.javaAPIobserver.displays.HeatIndexDisplay(
            weatherDataWithJavaAPI);

    weatherDataWithJavaAPI.gatherCurrentWeatherData();

  }
}
