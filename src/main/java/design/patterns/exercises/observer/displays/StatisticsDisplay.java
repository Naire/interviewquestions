package design.patterns.exercises.observer.displays;

import design.patterns.exercises.observer.interfaces.Observer;
import design.patterns.exercises.observer.interfaces.Subject;

public class StatisticsDisplay extends DisplayElement implements Observer {

  public StatisticsDisplay(Subject subject) {
    super(subject);
    subject.registerObserver(this);
  }

  @Override
  public void display() {
    System.out.println("Statistics display");
    System.out.printf("Stats temperature %.2f C, humidity %.2f, pressure %.2f %n", getTemperature(),
        getHumidity(), getPressure());
  }

  @Override
  public void update(double temperature, double humidity, double pressure) {
    setTemperature(temperature);
    setHumidity(humidity);
    setPressure(pressure);
    display();
  }
}
