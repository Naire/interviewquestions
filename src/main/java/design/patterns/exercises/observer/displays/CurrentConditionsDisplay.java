package design.patterns.exercises.observer.displays;

import design.patterns.exercises.observer.interfaces.Observer;
import design.patterns.exercises.observer.interfaces.Subject;

public class CurrentConditionsDisplay extends DisplayElement implements Observer {

  public CurrentConditionsDisplay(Subject subject) {
    super(subject);
    subject.registerObserver(this);
  }

  @Override
  public void display() {
    System.out.println("Current condition display");
    System.out
        .printf("Current temperature %.2f C, humidity %.2f %n", getTemperature(), getHumidity());
  }

  @Override
  public void update(double temperature, double humidity, double pressure) {
    setTemperature(temperature);
    setHumidity(humidity);
    display();
  }
}
