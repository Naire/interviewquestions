package design.patterns.exercises.observer.displays;

import design.patterns.exercises.observer.interfaces.Subject;

public abstract class DisplayElement {
  private double temperature;
  private double humidity;
  private double pressure;
  private Subject subject;


  public DisplayElement() {
  }

  public DisplayElement(Subject subject) {
    this.subject = subject;
  }

  public abstract void display();

  public double getTemperature() {
    return temperature;
  }

  public void setTemperature(double temperature) {
    this.temperature = temperature;
  }

  public double getHumidity() {
    return humidity;
  }

  public void setHumidity(double humidity) {
    this.humidity = humidity;
  }

  public double getPressure() {
    return pressure;
  }

  public void setPressure(double pressure) {
    this.pressure = pressure;
  }
}
