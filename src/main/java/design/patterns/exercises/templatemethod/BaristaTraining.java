package design.patterns.exercises.templatemethod;

import design.patterns.exercises.templatemethod.beverages.CaffeineBeverage;
import design.patterns.exercises.templatemethod.beverages.Coffee;
import design.patterns.exercises.templatemethod.beverages.Tea;

public class BaristaTraining {
  /**
   * Template method pattern.
   *
   * The Template Method defines the steps of an algorithm and allows subclasses to provide the
   * implementation for one or more steps.
   *
   * Used in Java API:
   *
   * - sorting Arrays (java.util): defined a static method sort() and deferred the comparison
   * (compareTo) part of the algorithm to the items being sorted. Not textbook pattern.
   *
   * - java.io has a read() method in InputStream that subclasses must implement and is used by the
   * template method read(byte b[], int off, int len).
   */
  public static void main(String[] args) {
    System.out.println("Coffee? Tea?");
    System.out.println("Tea");
    CaffeineBeverage beverage = new Tea();
    beverage.prepareRecipe();
    System.out.println("Coffee");
    beverage = new Coffee();
    beverage.prepareRecipe();
  }
}
