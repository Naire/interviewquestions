package design.patterns.exercises.templatemethod.beverages;

/**
 * This is the template method. Is serves as template of an algorithm. Each step is represented by a
 * method.
 */
public abstract class CaffeineBeverage {
  public final void prepareRecipe() { //this is final because the recipe is supposed to be common
    boilWater();
    brew();
    pourInCup();
    if (customerWantsCondiments()) {
      addCondiments();
    }
  }

  void pourInCup() {
    System.out.println("Pouring");
  }

  void boilWater() {
    System.out.println("Boiling water");
  }

  abstract void brew();

  abstract void addCondiments();

  /**
   * This method is called a hook - lets the subclass "hook into" the algorithm. The subclass can
   * override this but doesn't have to.
   */
  boolean customerWantsCondiments() {
    return true;
  }
}
