package design.patterns.exercises.templatemethod.beverages;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Coffee extends CaffeineBeverage {
  @Override
  void brew() {
    System.out.println("Brewing coffee");
  }

  @Override
  void addCondiments() {
    System.out.println("Adding milk and sugar");
  }

  @Override
  boolean customerWantsCondiments() {
    String answer = gerUserInput();
    return answer.toLowerCase().startsWith("y");
  }

  private String gerUserInput() {
    System.out.println("Would you like milk and sugar with your coffee?");
    String answer = null;
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      answer = reader.readLine();
      reader.close();
    } catch (IOException e) {
      System.out
          .printf("Unable to read your answer. You will be served coffee without condiments. %s%n",
              e.getMessage());
    }
    return answer == null ? "no" : answer;
  }
}
