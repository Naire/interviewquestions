package design.patterns.exercises.factory.exceptions;

public class PizzaTypeNotFoundException extends Exception {
  public PizzaTypeNotFoundException(String message) {
    super(message);
  }
}
