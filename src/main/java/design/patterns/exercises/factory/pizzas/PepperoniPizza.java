package design.patterns.exercises.factory.pizzas;

public class PepperoniPizza extends Pizza {
  private static String name = "PepperoniPizza";

  @Override
  public Pizza prepare() {
    getIngredients().add("pepperoni");
    System.out.printf("Preparing %s%n", name);
    System.out.printf("Ingredients: %s%n", getIngredients());
    return this;
  }

  @Override
  public Pizza bake() {
    System.out.printf("Baking %s%n", name);
    setBaked(true);
    System.out.println((isBaked()) ? "Pizza is done!" : "Pizza is still not baked");
    return this;
  }

  @Override
  public Pizza cut() {
    setNumberOfSlices(5);
    System.out.printf("Cutting %s into %s slices %n", name, getNumberOfSlices());
    return this;
  }

  @Override
  public Pizza box() {
    setBox("Packed in a red square box");
    System.out.println(this.getBox());
    return this;
  }

}
