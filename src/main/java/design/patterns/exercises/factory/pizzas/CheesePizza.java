package design.patterns.exercises.factory.pizzas;

public class CheesePizza extends Pizza {

  private static String name = "CheesePizza";
  private static int numberOfPieces = 8;

  @Override
  public Pizza prepare() {
    getIngredients().add("cheese");
    System.out.printf("Preparing %s%n", name);
    return this;
  }

  @Override
  public Pizza bake() {
    System.out.printf("Baking %s%n", name);
    setBaked(true);
    System.out.println((isBaked()) ? "Pizza is done!" : "Pizza is still not baked");
    return this;
  }

  @Override
  public Pizza cut() {
    setNumberOfSlices(numberOfPieces);
    System.out.printf("Cutting %s into %s slices %n", name, numberOfPieces);
    return this;
  }

  @Override
  public Pizza box() {
    setBox("Packed in a yellow square box");
    System.out.println(this.getBox());
    return this;
  }
}
