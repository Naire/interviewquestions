package design.patterns.exercises.factory.pizzas;

import java.util.ArrayList;

public abstract class Pizza {

  private final ArrayList<String> ingredients;
  private String box;
  private int numberOfSlices;
  private boolean baked = false;

  public Pizza() {
    ingredients = new ArrayList<>();
    ingredients.add("delicious dough");
    ingredients.add("tomato sauce");
  }

  public abstract Pizza prepare();

  public abstract Pizza bake();

  public abstract Pizza cut();

  public abstract Pizza box();

  public String getBox() {
    return box;
  }

  protected void setBox(String box) {
    this.box = box;
  }

  public int getNumberOfSlices() {
    return numberOfSlices;
  }

  protected void setNumberOfSlices(int numberOfSlices) {
    this.numberOfSlices = numberOfSlices;
  }

  public boolean isBaked() {
    return baked;
  }

  protected void setBaked(boolean baked) {
    this.baked = baked;
  }

  public ArrayList<String> getIngredients() {
    return ingredients;
  }

}
