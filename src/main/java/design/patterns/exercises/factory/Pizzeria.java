package design.patterns.exercises.factory;

import design.patterns.exercises.factory.exceptions.PizzaTypeNotFoundException;

public class Pizzeria {

  /**
   * Factory pattern.
   *
   * Instantiating concrete objects of one arbitrary type.
   *
   * Static vs non-static: Defining a simple factory as a static method is called a static factory.
   * No need of object to use the create method. Unable to subclass and change the behavior of the
   * create method.
   */

  public static void main(String[] args) {
    System.out.println("Pizzeria");
    PizzaStore store = new PizzaStore(new SimplePizzaFactory());
    try {
      store.orderPizza("pepperonipizza");
      System.out.println();
      store.orderPizza("cheesepizza");
    } catch (PizzaTypeNotFoundException e) {
      System.out.println(e.getMessage());
    }
  }

}
