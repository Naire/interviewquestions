package design.patterns.exercises.factory;

import java.util.Optional;
import design.patterns.exercises.factory.exceptions.PizzaTypeNotFoundException;
import design.patterns.exercises.factory.pizzas.Pizza;

public class PizzaStore {

  private SimplePizzaFactory factory;

  public PizzaStore(SimplePizzaFactory factory) {
    this.factory = factory;
  }

  public Pizza orderPizza(String type) throws PizzaTypeNotFoundException {
    Optional<Pizza> optionalPizza = factory.createPizza(type);
    if (optionalPizza.isPresent()) {
      return optionalPizza.get().prepare().bake().cut().box();
    }
    throw new PizzaTypeNotFoundException(String.format("There is no pizza of type %s", type));
  }
}
