package design.patterns.exercises.factory;

import java.util.Optional;
import java.util.TreeMap;
import java.util.function.Supplier;
import design.patterns.exercises.factory.pizzas.CheesePizza;
import design.patterns.exercises.factory.pizzas.PepperoniPizza;
import design.patterns.exercises.factory.pizzas.Pizza;

public class SimplePizzaFactory {

  private final static TreeMap<String, Supplier<Pizza>> inventory = new TreeMap<>();

  static {

    inventory.put("CHEESEPIZZA", CheesePizza::new);
    inventory.put("PEPPERONIPIZZA", PepperoniPizza::new);

  }

  public Optional<Pizza> createPizza(String type) {
    Supplier<Pizza> pizza = inventory.get(type.toUpperCase());
    return pizza == null ? Optional.empty() : Optional.of(pizza.get());
  }

}