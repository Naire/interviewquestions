package questions;

/**
 * Task: check whether a given number is a prime number.
 */
public class Prime {

  /**
   * A prime number is a natural number greater than 1 that cannot be formed by multiplying two
   * smaller natural numbers.
   * @param number to check.
   * @return true if given number is prime, false otherwise.
   */
  public static boolean isPrime(int number) {
    if (number <= 1) {
      return false;
    }

    double limit = Math.sqrt(number);

    for (int i = 2; i < limit; i++) {
      if (number % i == 0) {
        return false;
      }
    }
    return true;
  }
}
