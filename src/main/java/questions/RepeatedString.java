package questions;

/**
 * Task: given string s where 1<s.length()<=100 and long 1<n<10^12 return number of occurences of
 * letter "a" in n first characters of an infinite string created from s
 */
public class RepeatedString {

  public static long repeatedString(String s, long n) {
    if (!s.contains("a")) {
      return 0L;
    }
    long numberOfAinS = countA(s);
    if (numberOfAinS == s.length()) {
      return n;
    }
    long div = n / s.length();
    long remainder = n % s.length();
    return (numberOfAinS * div) + countA(s.substring(0, (int) remainder));

  }

  static long countA(String s) {
    return s.chars().filter(ascii -> ascii == 97).count();
  }
}