package questions;

/**
 * Binary search:
 *
 * The algorithm compares the value with the middle element of the array - if it is bigger then it
 * adjusts the search to the half containing smaller numbers, if it is smaller then takes the other
 * half, if equal the search is over. Array has to be sorted first.
 */
public class BinarySearch {

  /**
   * Binary search algorithm. Requires a sorted array - does not check whether it is sorted or not.
   *
   * @param sortedArray sorted array with data.
   * @param find element to look for
   * @return index of found element, -1 otherwise.
   */
  public static int binarySearch(int[] sortedArray, int find) {
    int index = -1;
    int lowIndex = 0;
    int highIndex = sortedArray.length - 1;
    while (lowIndex <= highIndex) {
      int middle = (highIndex + lowIndex) / 2;
      if (sortedArray[middle] > find) {
        highIndex = middle - 1;
      } else if (sortedArray[middle] < find) {
        lowIndex = middle + 1;
      } else if (sortedArray[middle] == find) {
        index = middle;
        break;
      }
    }
    return index;
  }
}
