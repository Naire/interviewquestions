package questions;

/**
 * Fibonacci number sequence starts with: 1 1. Up from the third number, next number in the sequence
 * is a sum of two previous ones.
 *
 * Task: calculate nth Fibonacci number in both iterative and
 * recursive way. Maximum is 92th Fibonacci's number due to result being a long.
 */
public class Fibonacci {
  /**
   * Iterative method for calculating Fibonacci number.
   * @param whichNumber parameter telling which Fibonacci number to calculate. Should be > 0 and <
   * 93.
   * @return -1 if the parameter was invalid, nth Fibonacci number otherwise.
   */
  public static long iterative(int whichNumber) {
    if (whichNumber <= 0 || whichNumber > 93) {
      return -1;
    }
    if (whichNumber == 1 || whichNumber == 2) {
      return 1;
    }
    long fib = 0;
    long prev = 1;
    long next = 1;
    for (int step = 3; step <= whichNumber; step++) {
      fib = prev + next;
      prev = next;
      next = fib;
    }
    return fib;
  }


  /**
   * Recursive method for calculating Fibonacci number.
   * @param whichNumber parameter telling which of Fibonacci numbers to calculate. Should be > 0 and
   * < 41 (set arbitrarily so that the test can finish in a reasonable time).
   * @return -1 if the parameter was invalid, nth Fibonacci number otherwise.
   */
  public static long recursive(int whichNumber) {
    if (whichNumber <= 0 || whichNumber > 40) {
      return -1;
    }
    if (whichNumber == 1 || whichNumber == 2) {
      return 1;
    }
    return recursive(whichNumber - 1) + recursive(whichNumber - 2);
  }
}

