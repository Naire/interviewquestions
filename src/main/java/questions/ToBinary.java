package questions;

import java.util.ArrayDeque;
import java.util.LinkedList;

public class ToBinary {

  /**
   * Returns binary representation of a given int using Integer.toBinaryString method.
   *
   * @param number integer > 0
   * @return binary representation in a String
   */
  public static String toBinaryWithLib(int number) {
    return Integer.toBinaryString(number);
  }

  /**
   * Returns binary representation of a given int using division by 2 and a StringBuilder.
   *
   * @param number integer > 0
   * @return binary representation in a String
   */
  public static String toBinary(int number) {
    if (number < 0) {
      throw new IllegalArgumentException();
    }
    StringBuilder sb = new StringBuilder();
    int div = number;
    do {
      sb.append(String.valueOf(div % 2));
    } while ((div /= 2) >= 1);
    return sb.reverse().toString();
  }

  /**
   * Returns binary representation of a given int using division by 2 and a ArrayDeque.
   *
   * @param number integer > 0
   * @return binary representation in a String
   */
  public static ArrayDeque<Boolean> toBinaryArrayDeque(int number) {
    if (number < 0) {
      throw new IllegalArgumentException();
    }
    ArrayDeque<Boolean> result = new ArrayDeque<>();
    int div = number;
    do {
      result.push((div % 2) > 0);
    } while ((div /= 2) >= 1);
    return result;
  }

  /**
   * Returns binary representation of a given int using division by 2 and a LinkedList.
   *
   * @param number integer > 0
   * @return binary representation in a String
   */
  public static LinkedList<Boolean> toBinaryLinkedList(int number) {
    if (number < 0) {
      throw new IllegalArgumentException();
    }
    LinkedList<Boolean> result = new LinkedList<>();
    int div = number;
    do {
      result.push((div % 2) > 0);
    } while ((div /= 2) >= 1);
    return result;
  }

  /**
   * Returns a XOR of binary representations of given integers. Turns into binary by using division
   * by 2 and a StringBuilder.
   *
   * @param a integer > 0
   * @param b integer > 0
   * @return binary representation of a xor operation on given arguments, in a String
   */
  public static String xorWithDiv(int a, int b) {
    if (a < 0 || b < 0) {
      throw new IllegalArgumentException();
    }
    StringBuilder sb = new StringBuilder();
    int divA = a;
    int divB = b;
    do {
      sb.append((divA % 2) != (divB % 2) ? "1" : "0");
    } while (((divA /= 2) >= 1) | (divB /= 2) >= 1);
    return sb.reverse().toString();
  }
  
}