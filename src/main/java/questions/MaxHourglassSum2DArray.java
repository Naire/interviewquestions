package questions;

/*
 Given a NxN 2D Array for example:

 1 1 1 0 0 0
 0 1 0 0 0 0
 1 1 1 0 0 0
 0 0 0 0 0 0
 0 0 0 0 0 0
 0 0 0 0 0 0

 We define an hourglass to be a subset of values with indices falling in this pattern in 's graphical representation:

 a b c
   d
 e f g

 An hourglass sum is the sum of an hourglass' values. Calculate the hourglass sum for every hourglass in the array, then print the maximum hourglass sum.
 */
public class MaxHourglassSum2DArray {

  public static int hourglassSum(int[][] arr) {
    int sum = calculateHourglassSum(arr, 0, 0);
    for (int i = 0; i < arr.length - 2; i++) {
      for (int j = 0; j < arr.length - 2; j++) {
        int currentHourglassSum = calculateHourglassSum(arr, i, j);
        if (sum < currentHourglassSum) {
          sum = currentHourglassSum;
        }
      }
    }
    return sum;
  }

  private static int calculateHourglassSum(int[][] arr, int i, int j) {
    return arr[i][j] + arr[i][j + 1] + arr[i][j + 2]
        + arr[i + 1][j + 1]
        + arr[i + 2][j] + arr[i + 2][j + 1] + arr[i + 2][j + 2];

  }
}
