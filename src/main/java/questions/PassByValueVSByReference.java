package questions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Is Java pass by value or pass by reference?
 */
public class PassByValueVSByReference {
  public static void main(String[] args) {
    System.out.println("Is Java pass by value or pass by reference?");

    Colorful red = new Colorful("Red");
    Colorful alsoRed = new Colorful("Red");
    Colorful blue = new Colorful("Blue");

    System.out.println("We have a list of colorful objects:");
    System.out
        .println(red.getColor() + ": " + red
            + ", " + alsoRed.getColor()
            + ": " + alsoRed + ", "
            + blue.getColor() + ": " + blue);

    System.out.println("Calling swap method which will assign blue to red one and vice versa.");
    swap(red, blue);
    System.out.println("After swapping:");
    System.out.println("red: " + red);
    System.out.println("blue: " + blue);
    System.out.println("The references were not affected.");

    System.out.println("Calling a change method with " + blue + ", color: " + blue.getColor());
    change(blue);
    System.out.println("Colorful after change: " + blue + ", color: " + blue.getColor());

    System.out.println("----Primitives---");
    int x = 5, y = 7;
    System.out.println("We have ints x = " + x + ", y = " + y);
    System.out.println("Calling swap(x ,y)");
    swapPrimitives(x, y);
    System.out.println("Ints after swap x = " + x + ", y = " + y);
    System.out.println("Calling change method for int = " + x);
    changePrimitive(x);
    System.out.println("After method int = " + x);

    System.out.println("Answer: Java is pass by value in a sense that a copy of reference "
        + "is passed into a method.");
    System.out.println("It is possible to use that reference to set some attributes "
        + "on the object but it is not possible to reassign a reference.");
    System.out.println("Primitives are just values.");

  }

  private static void change(Colorful colorful) {
    System.out.println("---change method---");
    System.out.println("Given: " + colorful + ", color: " + colorful.getColor());
    colorful.setColor("Red");
    System.out.println("Set color to red.");
    colorful = new Colorful("Green");
    System.out.println("Assigned to the argument a new object: " + colorful);
    colorful.setColor("Yellow");
    System.out.println("Set color of new object to " + colorful.getColor() + ": " + colorful);
    System.out.println("---end of change method---");
  }


  private static void swap(Colorful o1, Colorful o2) {
    Colorful temp = o1;
    o1 = o2;
    o2 = temp;
  }

  private static void swapPrimitives(int a, int b) {
    int temp = a;
    a = b;
    b = a;
  }

  private static void changePrimitive(int a) {
    System.out.println("---start of method---");
    System.out.println("Changing int = " + a);
    a = 42;
    System.out.println("Changed int to: " + a);
    System.out.println("---end of method---");
  }
}

@Setter
@Getter
@AllArgsConstructor
class Colorful {
  private String color;
}
