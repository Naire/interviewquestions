package questions;

/**
 * Question: how to check that file which was sent over the network has not be tempered with during
 * the transfer? Answer: check the checksum (md5). Task: write a method that takes in long and
 * returns true if it contains even number of 1 (in binary) or false otherwise.
 */
public class CheckFileUncorrupted {

  public static boolean hasEvenNumberOfOnes(long data) {
    int sum = 0;
    for(int i = 0; i < 64; i++) {
      if((data & 1) == 1) {
        sum++;
      }
      data = data >> 1;
    }
    if(sum % 2 == 0) {
      return true;
    }
    return false;
  }

}
