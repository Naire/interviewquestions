package questions;

import java.util.ArrayList;
import java.util.List;

/**
 * Task: write permutations of a given String.
 */
public class Permutations {

  private List<String> result;


  public List<String> permutations(String str) {
    result = new ArrayList<>();
    permutation("", str);
    return result;
  }

  private void permutation(String prefix, String str) {
    if (str.length() == 0) {
      result.add(prefix);
    } else {
      int n = str.length();
      for (int i = 0; i < n; i++) {
        permutation(
            prefix + str.charAt(i),
            str.substring(0, i) + str.substring(i + 1, n)
        );
      }
    }
  }

  public static void main(String[] args) {
    Permutations p = new Permutations();
    String word = "word";
    System.out.printf("Permutations of '%s' are %s", word, p.permutations(word));
  }

}
