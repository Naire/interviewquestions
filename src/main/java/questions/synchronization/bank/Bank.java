package questions.synchronization.bank;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Task: write code to transfer money between accounts. A random amount of money should be
 * transferred from specified account to a random account. Total sum of money in the bank have to be
 * constant. Each account should be a separate Thread. It is ok to use double for keeping money. At
 * the beginning all accounts will have the same amount of money.
 */
public class Bank {

  private final Account[] accounts;
  private final Lock lock;
  private final Condition sufficientFunds;

  public Bank(int numberOfAccounts, double initialMoneyForEachAccount) {
    accounts = createAccounts(numberOfAccounts);
    setBalanceOnAccounts(initialMoneyForEachAccount);
    lock = new ReentrantLock();
    sufficientFunds = lock.newCondition();
  }

  /**
   * Transfers money between accounts. If there is not enough money in the account, the thread will
   * wait for the balance to be sufficient while freeing the lock. Assumes that accounts with
   * specified numbers exist.
   *
   * @param fromAccountNumber number of account from which money will be withdrawn.
   * @param toAccountNumber number of account to which money will be transferred.
   * @param amount of money to transfer.
   * @return total sum of bank balance after this transfer.
   * @throws InterruptedException because of java.util.concurrent.locks.Condition#await method
   * usage.
   */
  public double transfer(int fromAccountNumber, int toAccountNumber, double amount)
      throws InterruptedException, TimeoutException {
    lock.lock();
    try {
      while (accounts[fromAccountNumber].getBalance() < amount) {
        final boolean timeout = sufficientFunds.await(5, TimeUnit.SECONDS);
        if(timeout) {
          lock.unlock();
          throw new TimeoutException(Thread.currentThread() + " has timed out.");
        }
      }
     // System.out.println(Thread.currentThread() + " will do the transfer.");
      final double prevFromBalance = accounts[fromAccountNumber].getBalance();
      accounts[fromAccountNumber].setBalance(prevFromBalance - amount);
      final double prevToBalance = accounts[toAccountNumber].getBalance();
      accounts[toAccountNumber].setBalance(prevToBalance + amount);
     // System.out.printf("Transferred %10.2f from %s to %s\n", amount, accounts[fromAccountNumber],
    //      accounts[toAccountNumber]);
    //  System.out
    //      .printf("Total sum of money in the bank after transfer: %10.2f\n", getTotalBankBalance());
      return getTotalBankBalance();
    } finally {
      sufficientFunds.signalAll();
      lock.unlock();
    }
  }

  /**
   * @return how many accounts there are in this bank.
   */
  public int getNumberOfAccounts() {
    return accounts.length;
  }

  /**
   * @param numberOfAccounts how many accounts to create.
   * @return array of Account objects with unique number and name set.
   */
  private Account[] createAccounts(int numberOfAccounts) {
    final Account[] result = new Account[numberOfAccounts];
    for (int i = 0; i < numberOfAccounts; i++) {
      result[i] = new Account(i, String.format("Account number: %d", i));
    }
    return result;
  }

  /**
   * Initializes accounts with the same amount of money.
   *
   * @param initialMoneyForEachAccount amount of money per account.
   */
  private void setBalanceOnAccounts(double initialMoneyForEachAccount) {
    for (Account account : accounts) {
      account.setBalance(initialMoneyForEachAccount);
    }
  }

  /**
   * @return total sum of money in this bank.
   */
  private double getTotalBankBalance() {
    lock.lock();
    try {
      return Arrays.stream(accounts).mapToDouble(Account::getBalance).sum();
    } finally {
      lock.unlock();
    }
  }
}
