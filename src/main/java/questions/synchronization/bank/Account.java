package questions.synchronization.bank;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@RequiredArgsConstructor
public class Account {

  private final int number;
  private final String name;
  @Setter
  private double balance;
}
