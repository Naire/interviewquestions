package questions;

/**
 * Write a short program that prints each number from 1 to 100 on a new line.
 *
 * For each multiple of 3, print "Fizz" instead of the number.
 *
 * For each multiple of 5, print "Buzz" instead of the number.
 *
 * For numbers which are multiples of both 3 and 5, print "FizzBuzz" instead of the number.
 */
public class FizzBuzz {

  public static void main(String[] args) {
    FizzBuzz(1, 100);
  }

  private static void FizzBuzz(int min, int max) {
    for (int number = min; number <= max; number++) {
      boolean printNumber = true;
      StringBuilder sb = new StringBuilder();
      if (number % 3 == 0) {
        sb.append("Fizz");
        printNumber = false;
      }
      if (number % 5 == 0) {
        sb.append("Buzz");
        printNumber = false;
      }
      if (printNumber) {
        sb.append(String.valueOf(number));
      }
      System.out.println(sb.toString());
    }
  }
}
