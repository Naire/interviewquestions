package questions;

import java.util.HashSet;
import java.util.Set;

/**
 * Check if given a sudoku board 9x9 it is a valid sudoku. Board has '.' if the field is empty,
 * number otherwise.
 *
 * Sudoku is valid if given numbers are not duplicated in a row, column and 3x3 square.
 */
public class Sudoku {
  public static boolean isValidSudoku(char[][] board) {
    final Set<String> seen = new HashSet<>();
    for (int row = 0; row < 9; row++) {
      for (int column = 0; column < 9; column++) {
        char number = board[row][column];
        if (number != '.') {
          //set.add returns true if this set did not already contain the specified element
          if (!seen.add(number + " in row " + row) ||
              !seen.add(number + " in column " + column) ||
              !seen.add(number + " in block " + row / 3 + "-" + column / 3)) {
            return false;
          }
        }
      }
    }
    return true;
  }
}
