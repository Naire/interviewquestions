package questions;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Task: given a list of integers return 3 max values.
 */
public class MaxThreeFromList {
  /**
   * Sorts a list of Integers using Java 8 stream API. Displays time in microseconds.
   * @param list of Integers to be sorted.
   * @return list of 3 max integers.
   */
  public static List<Integer> getMaxThreeUsingStreams(List<Integer> list) {
    long start = System.nanoTime();
    final List<Integer> result = list.stream()
        .sorted(getDescComparator())
        .limit(3)
        .collect(Collectors.toList());
    long end = System.nanoTime();
    long time = (end - start) / 1000;
    System.out.println("Using streams, time: " + time + " microseconds");
    return result;
  }

  /**
   * Sorts a list using Java's Collection sort. Displays time in microseconds. Does not change the
   * initial list.
   * @param listToSort of Integers to be sorted.
   * @return list of 3 max integers.
   */
  public static List<Integer> getMaxThreeUsingCollectionSort(List<Integer> listToSort) {
    long start = System.nanoTime();
    List<Integer> list = new ArrayList<>(listToSort);
    list.sort(getDescComparator());
    List<Integer> result = new ArrayList<>(3);
    for (int i = 0; i < 3; i++) {
      result.add(list.get(i));
    }
    long end = System.nanoTime();
    long time = (end - start) / 1000;
    System.out.println("Using collection sort, time: " + time + " microseconds");
    return result;
  }

  /**
   * Sorts a list using quicksort algorithm. Copies data into an array first - does not change the
   * initial list. Displays time in microseconds.
   * @param listToSort of Integers to be sorted.
   * @return list of 3 max integers.
   */
  public static List<Integer> getMaxThreeUsingQuickSort(List<Integer> listToSort) {
    long start = System.nanoTime();
    final int size = listToSort.size();
    int[] elements = new int[size];
    for (int i = 0; i < size; i++) {
      elements[i] = listToSort.get(i);
    }

    quickSort(elements, 0, size - 1);

    List<Integer> result = new ArrayList<>(3);
    for (int i = 0; i < 3; i++) {
      result.add(elements[i]);
    }
    long end = System.nanoTime();
    long time = (end - start) / 1000;
    System.out.println("Using quicksort, time: " + time + " microseconds");
    return result;
  }

  /**
   * Implementation of quicksort algorithm. Changes initial array of elements.
   */
  private static void quickSort(int[] elements, int leftIndex, int rightIndex) {
    int currentLeft, currentRight, middle, temp;

    currentLeft = leftIndex;
    currentRight = rightIndex;
    middle = elements[(leftIndex + rightIndex) / 2];
    do {
      while (elements[currentLeft] > middle) {
        currentLeft++;
      }
      while (elements[currentRight] < middle) {
        currentRight--;
      }
      if (currentLeft <= currentRight) {
        temp = elements[currentLeft];
        elements[currentLeft] = elements[currentRight];
        elements[currentRight] = temp;
        currentLeft++;
        currentRight--;
      }
    }
    while (currentLeft <= currentRight);
    if (leftIndex < currentRight) {
      quickSort(elements, leftIndex, currentRight);
    }
    if (currentLeft < rightIndex) {
      quickSort(elements, currentLeft, rightIndex);
    }
  }

  /**
   * Returns a lambda with a descending comparator of integers.
   */
  private static Comparator<Integer> getDescComparator() {
    return (a, b) -> {
      if (a > b) {
        return -1;
      } else if (a < b) {
        return 1;
      }
      return 0;
    };
  }
}
