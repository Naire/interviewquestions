package questions;

/**
 * Task: check whether the given String is a palindrome.
 */
public class Palindrome {
  //Comparisons: n/2
  //compares characters from both ends

  /**
   * A string is a palindrome if it remains unchanged when reversed. This method is
   * case-insensitive.
   * @param data String to be checked.
   * @return true if given String is a palindrome, false otherwise.
   */
  public static boolean isPalindrome(String data) {
    data = data.toLowerCase();
    int length = data.length();
    for (int i = 0; i < length / 2; ++i) {
      if (data.charAt(i) != data.charAt(length - i - 1)) {
        return false;
      }
    }
    return true;
  }
}
