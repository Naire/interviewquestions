package questions;

public class UniqueChars {

  /**
   * Task: check if the given String has all characters unique. You can use only one data
   * structure.
   *
   * This solution is O(n^2)
   */
  public static String areAllUniqueCharsStringVersion(final String data) {
    if (data == null || data.isEmpty()) {
      return "Data is empty.";
    }

    StringBuilder chars = new StringBuilder(String.valueOf(data.charAt(0)));
    for (int i = 1; i < data.length(); i++) {
      String current = String.valueOf(data.charAt(i));
      if (chars.toString().contains(current)) {
        return "NO";
      }
      chars.append(current);
    }
    return "YES";
  }

  /**
   * Task: check if the given String has all characters unique. You can use only one data
   * structure.
   *
   * This solution is O(n)
   */
  public static boolean areAllUniqueCharsBooleanVersion(final String data) {
    if (data == null || data.isEmpty()) {
      return true;
    }
    boolean[] char_set = new boolean[256]; // we assume ASCII
    for (int i = 0; i < data.length(); i++) {
      int index = data.charAt(i);
      if (char_set[index]) {
        return false;
      }
      char_set[index] = true;
    }
    return true;
  }

  /**
   * Task: remove duplicate characters from String.
   */
  public static String removeDuplicateChars(String data) {
    if (data == null || data.isEmpty()) {
      return "Data is empty";
    }
    boolean[] char_set = new boolean[256]; // we assume ASCII
    for (int i = 0; i < data.length(); i++) {
      int asciiValue = data.charAt(i);
      if (char_set[asciiValue]) {
        data = removeGivenIndex(data, data.lastIndexOf(data.charAt(i)));
        i--;
      }
      char_set[asciiValue] = true;
    }
    return data;
  }

  private static String removeGivenIndex(final String data, int index) {
    if (index == 0) {
      return data.substring(1);
    }
    int dataLength = data.length();
    if (index == dataLength) {
      return data.substring(dataLength - 1);
    }
    return data.substring(0, index) + data.substring(index + 1);
  }
}
