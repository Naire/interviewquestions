package questions;

public class ZeroSum {
  /**
   * Given a number 1 < n < 100 return an array of unique integers which sum up to 0.
   */
  public static int[] arrayOfOpposites(int n) {
    if (n <= 1 || n >= 100) {
      throw new IllegalArgumentException("n is out of range. Should be 1 < n < 100");
    }
    int[] result = new int[n];
    for (int i = 0, number = 1; i < n - 1; i = i + 2, number++) {
      result[i] = number;
      result[i + 1] = number * (-1);
    }

    if ((n % 2) == 1) {
      result[n - 1] = 0;
    }
    return result;
  }
}
