Repository for interview exercises.

Autor: Agnieszka Pająk

This repository is public, feel free to copy my solutions but please be aware they might not be "the perfect answer".
Design patterns part is very strongly connected with a book: Head first design patterns - I do not claim any right to those, they are purely an exercise and no money is being made.